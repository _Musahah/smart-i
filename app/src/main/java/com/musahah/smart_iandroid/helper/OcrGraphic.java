/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.musahah.smart_iandroid.helper;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.vision.text.TextBlock;
import com.musahah.smart_iandroid.MyLib.MyLib;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.Utils.SharedPrefUtil;
import com.musahah.smart_iandroid.helper.Constants;
import com.musahah.smart_iandroid.ui.camera.GraphicOverlay;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Graphic instance for rendering TextBlock position, size, and ID within an associated graphic
 * overlay view.
 */

public class OcrGraphic extends GraphicOverlay.Graphic {
    private static int TEXT_COLOR;

    private static Paint sRectPaint;
    private static Paint sTextPaint;
    private TextBlock mText;
    private TextBlock mNextText = null;
    private Context context;
    private String ID = "";

    OcrGraphic(GraphicOverlay overlay, TextBlock text, Context c, String identity) {
        super(overlay);
        mText = text;
        context = c;
        ID = identity;

        TEXT_COLOR = context.getResources().getColor(R.color.theme_center);

        if (sRectPaint == null) {
            sRectPaint = new Paint();
            sRectPaint.setColor(TEXT_COLOR);
            sRectPaint.setStyle(Paint.Style.STROKE);
            sRectPaint.setStrokeWidth(4.0f);
        }

        if (sTextPaint == null) {
            sTextPaint = new Paint();
            sTextPaint.setColor(TEXT_COLOR);
            sTextPaint.setTextSize(54.0f);
        }
        // Redraw the overlay, as this graphic has been added.
        postInvalidate();
    }

    OcrGraphic(GraphicOverlay overlay, TextBlock text, TextBlock nextText, Context c, String identity) {
        super(overlay);
        mText = text;
        mNextText = nextText;
        context = c;

        ID = identity;

        if (sRectPaint == null) {
            sRectPaint = new Paint();
            sRectPaint.setColor(TEXT_COLOR);
            sRectPaint.setStyle(Paint.Style.STROKE);
            sRectPaint.setStrokeWidth(4.0f);
        }

        if (sTextPaint == null) {
            sTextPaint = new Paint();
            sTextPaint.setColor(TEXT_COLOR);
            sTextPaint.setTextSize(54.0f);
        }
        // Redraw the overlay, as this graphic has been added.
        postInvalidate();
    }

    /**
     * Checks whether a point is within the bounding box of this graphic.
     * The provided point should be relative to this graphic's containing overlay.
     * @param x An x parameter in the relative context of the canvas.
     * @param y A y parameter in the relative context of the canvas.
     * @return True if the provided point is contained within this graphic's bounding box.
     */
    public boolean contains(float x, float y) {
        // TODO: Check if this graphic's text contains this point.

        if (mText == null) {
            return false;
        }
        RectF rect = new RectF(mText.getBoundingBox());
        rect.left = translateX(rect.left);
        rect.top = translateY(rect.top);
        rect.right = translateX(rect.right);
        rect.bottom = translateY(rect.bottom);
        Canvas c = new Canvas();
        c.drawRect(rect, sRectPaint);
        return (rect.left < x && rect.right > x && rect.top < y && rect.bottom > y);
    }

    /**
     * Draws the text block annotations for position, size, and raw value on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        // TODO: Draw the text onto the canvas.

        // Draws the bounding box around the TextBlock.
        RectF rect = new RectF(mText.getBoundingBox());
        rect.left = translateX(rect.left);
        rect.top = translateY(rect.top);
        rect.right = translateX(rect.right);
        rect.bottom = translateY(rect.bottom);
        canvas.drawRect(rect, sRectPaint);

        if (mNextText != null) {
            RectF rect2 = new RectF(mNextText.getBoundingBox());
            rect2.left = translateX(rect2.left);
            rect2.top = translateY(rect2.top);
            rect2.right = translateX(rect2.right);
            rect2.bottom = translateY(rect2.bottom);
            canvas.drawRect(rect2, sRectPaint);
        }

        final Handler handler = new Handler();
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        rechargeCard();
                    }
                });
            }
        }, 800);
    }

    private void rechargeCard() {
        String cardNumber = mText.getValue();
        cardNumber = cardNumber.replace(" ", "");
        String encodedHash = Uri.encode("#");
        String toRecharge = SharedPrefUtil.getInstance().getDialerCodeSelected() + cardNumber + encodedHash;

        // Save cardnumber on clip board if its on
        if(SharedPrefUtil.getInstance().getCopyScanned()) {
            MyLib.System.setClipboard(context, cardNumber);
        }

        dialToRecharge(toRecharge);
    }

    private void dialToRecharge(String toRecharge) {
        Intent callIntent;

        if(SharedPrefUtil.getInstance().getAutoDial()) // If auto dial is on
            callIntent = new Intent(Intent.ACTION_CALL);
        else
            callIntent = new Intent(Intent.ACTION_DIAL);

        callIntent.setData(Uri.parse("tel:" + toRecharge));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(callIntent);

        android.os.Process.killProcess(android.os.Process.myPid());
    }


//    private void friendiCardRecharge() {
//        String cardNumber = mText.getValue();
//
//        // START Converting text to orignal Friendi voucher number
//        Pattern pattern = Pattern.compile(Constants.MATCHPATTERN.FRIENDI_PLASTIC_CARD_MATCH_PATTERN);
//        Matcher matcher = pattern.matcher(cardNumber);
//        if (matcher.find()) {
//            cardNumber = matcher.group(1);
//        }
//        // END Converting text to orignal Friendi voucher number
//        cardNumber = cardNumber.replace(" ", "");
//        String encodedHash = Uri.encode("#");
//
//        String toRecharge = Constants.DIALERCODE.FRIENDI_DIALERCODE + cardNumber + "*" + ID + encodedHash;
//        Log.i("TAG", "TO RECHARGE FRIENDI CARD: \n" + toRecharge);
//
//        dialToRecharge(toRecharge);
//    }
//
//    private void lebaraCardRecharge() {
//
//        String cardNumber = mText.getValue();
//        cardNumber = cardNumber.replace(" ", "");
//        String encodedHash = Uri.encode("#");
//
//        String toRecharge = Constants.DIALERCODE.LEBARA_DIALERCODE + cardNumber + "*" + ID + encodedHash;
//        Log.i("TAG", "TO RECHARGE LEBARA CARD: \n" + toRecharge);
//
//        dialToRecharge(toRecharge);
//    }
//
//
//    private void mobilyCardRecharge() {
//
//        String cardNumber = mText.getValue();
//        cardNumber = cardNumber.replace(" ", "");
//        String encodedHash = Uri.encode("#");
//
//        String toRecharge = Constants.DIALERCODE.MOBILY_DIALERCODE + cardNumber + "*" + ID + encodedHash;
//        Log.i("TAG", "TO RECHARGE MOBILY CARD: \n" + toRecharge);
//
//        dialToRecharge(toRecharge);
//    }

//    private void zainCardRecharge() {
//        String nextValue;
//        if (mNextText != null)
//            nextValue = mNextText.getValue();
//        else
//            nextValue = "";
//
//        String cardNumber = mText.getValue();
//        cardNumber = cardNumber.replace(" ", "");
//        String encodedHash = Uri.encode("#");
//
//        String toRecharge = Constants.DIALERCODE.ZAIN_DIALERCODE + ID + "*" + cardNumber + encodedHash;
//        Log.i("TAG", "TO RECHARGE ZAIN CARD: \n" + toRecharge);
//
//        dialToRecharge(toRecharge);
//    }
//
//    private void sawaCardRecharge() {
//        String nextValue;
//        if (mNextText != null)
//            nextValue = mNextText.getValue();
//        else
//            nextValue = "";
//
//        String cardNumber = mText.getValue() + nextValue;
//        cardNumber = cardNumber.replace(" ", "");
//        String encodedHash = Uri.encode("#");
//
//        String toRecharge = Constants.DIALERCODE.SAWA_DIALERCODE  + cardNumber + "*" + ID + encodedHash;
//        Log.i("TAG", "TO RECHARGE SAWA CARD: \n" + toRecharge);
//
//        dialToRecharge(toRecharge);
//    }


}
