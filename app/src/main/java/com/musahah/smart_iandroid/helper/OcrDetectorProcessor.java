/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.musahah.smart_iandroid.helper;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.musahah.smart_iandroid.Activities.OcrCaptureActivity;
import com.musahah.smart_iandroid.Utils.SharedPrefUtil;
import com.musahah.smart_iandroid.ui.camera.CameraSourcePreview;
import com.musahah.smart_iandroid.ui.camera.GraphicOverlay;

/**
 * A very simple Processor which gets detected TextBlocks and adds them to the overlay
 * as OcrGraphics.
 * TODO: Make this implement Detector.Processor<TextBlock> and add text to the GraphicOverlay
 */
public class OcrDetectorProcessor extends OcrCaptureActivity implements Detector.Processor<TextBlock>{

    private GraphicOverlay<OcrGraphic> mGraphicOverlay;
    private CameraSourcePreview cameraPreview;
    public Context context;
    public String identity, SimOperatorName;
    private TextBlock nextItem;

    public OcrDetectorProcessor(GraphicOverlay<OcrGraphic> ocrGraphicOverlay, CameraSourcePreview cPreview, Context c, String ID, String SOName) {
        mGraphicOverlay = ocrGraphicOverlay;
        cameraPreview = cPreview;
        context = c;
        identity = ID;
        SimOperatorName = SOName;
    }

    @Override
    public void release() {
        mGraphicOverlay.clear();
    }

    @Override
    public void receiveDetections(Detector.Detections<TextBlock> detections) {
        mGraphicOverlay.clear();

        SparseArray<TextBlock> items = detections.getDetectedItems();

        for (int i = 0; i < items.size(); ++i) {
            TextBlock item = items.valueAt(i);

            if(i+1 == items.size()){}
            else
                nextItem = items.valueAt(++i);

            if (item != null && item.getValue() != null) {
                Log.d("Processor", "Text detected! " + item.getValue() );
            }

            if(item.getValue().matches(SharedPrefUtil.getInstance().getPatternSelected())){
                OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, identity);
                mGraphicOverlay.add(graphic);

                //Toast.makeText(context, "Scanned successfully!", Toast.LENGTH_SHORT).show();
            }

            ///////////////////////// ZAIN SIM //////////////////////////////////
//            if(SimOperatorName.equals(Constants.SIMOPERATORNAME.ZAIN_SIM_OPERATOR_NAME)){
//                //For ZAIN_PLASTIC_CARD_TYPE
//                if(item.getValue().matches(Constants.CARDPATTERN.ZAIN_PLASTIC_CARD_PATTERN)){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.ZAIN_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//                //For ZAIN_PAPER_CARD_TYPE
//                else if(item.getValue().matches(Constants.CARDPATTERN.ZAIN_PAPER_CARD_PATTERN)){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.ZAIN_PAPER_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//            }
//
//            ///////////////////////// SAWA SIM //////////////////////////////////
//            else if (SimOperatorName.equals(Constants.SIMOPERATORNAME.SAWA_SIM_OPERATOR_NAME)){
//                //For SAWA_PAPER_CARD_TYPE
//                if(item.getValue().matches(Constants.CARDPATTERN.SAWA_PAPER_CARD_PATTERN)){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.SAWA_PAPER_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//                //For SAWA_PLASTIC_CARD_TYPE
//                else if((item.getValue().matches("^[\\d]{7}$") && nextItem.getValue().matches("^[\\d]{8}$"))){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, nextItem, context, Constants.CARDTYPE.SAWA_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//                else if(item.getValue().matches(Constants.CARDPATTERN.SAWA_PLASTIC_CARD_PATTERN)){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.SAWA_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//            }
//
//            ///////////////////////// MOBILY SIM //////////////////////////////////
//            else if (SimOperatorName.equals(Constants.SIMOPERATORNAME.MOBILY_SIM_OPERATOR_NAME)){
//                //For MOBILY_PLASTIC_CARD_TYPE
//                if(item.getValue().matches(Constants.CARDPATTERN.MOBILY_PLASTIC_CARD_PATTERN)){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.MOBILY_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//                //For MOBILY_PAPER_CARD_TYPE
//                else if(item.getValue().matches(Constants.CARDPATTERN.MOBILY_PAPER_CARD_PATTERN)){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.MOBILY_PAPER_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//            }
//
//            ///////////////////////// LEBARA SIM //////////////////////////////////
//            else if (SimOperatorName.equals(Constants.SIMOPERATORNAME.LEBARA_SIM_OPERATOR_NAME)){
//                //For LEBARA_PLASTIC_CARD_TYPE
//                if(item.getValue().matches(Constants.CARDPATTERN.LEBARA_PLASTIC_CARD_PATTERN)){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.LEBARA_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//            }
//
//            ///////////////////////// FRIENDI SIM //////////////////////////////////
//            else if (SimOperatorName.equals(Constants.SIMOPERATORNAME.FRIENDI_SIM_OPERATOR_NAME)){
//                //For FRIENDI_PLASTIC_CARD_TYPE
//
//                if(item.getValue().matches(Constants.CARDPATTERN.FRIENDI_PLASTIC_CARD_PATTERN)){
//
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.FRIENDI_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//
//            }

            //For TEST
//            if(item.getValue().matches(Constants.CARDPATTERN.SAWA_PAPER_CARD_PATTERN)){
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.SAWA_PAPER_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//                 if((item.getValue().matches("^[\\d]{7}$") && nextItem.getValue().matches("^[\\d]{8}$"))){
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, nextItem, context, Constants.CARDTYPE.SAWA_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//                if(item.getValue().matches(Constants.CARDPATTERN.SAWA_PLASTIC_CARD_PATTERN)){
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.SAWA_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//             if(item.getValue().matches(Constants.CARDPATTERN.ZAIN_PLASTIC_CARD_PATTERN)){
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.ZAIN_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//            if(item.getValue().matches(Constants.CARDPATTERN.MOBILY_PLASTIC_CARD_PATTERN)){
//                    OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.MOBILY_PLASTIC_CARD_TYPE, identity);
//                    mGraphicOverlay.add(graphic);
//                }
//            if(item.getValue().matches(Constants.CARDPATTERN.FRIENDI_PLASTIC_CARD_PATTERN)){
//                OcrGraphic graphic = new OcrGraphic(mGraphicOverlay, item, context, Constants.CARDTYPE.FRIENDI_PLASTIC_CARD_TYPE, identity);
//                mGraphicOverlay.add(graphic);
//            }

        }
    }

}
