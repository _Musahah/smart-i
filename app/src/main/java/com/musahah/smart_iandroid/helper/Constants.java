package com.musahah.smart_iandroid.helper;

/**
 * Created by shuja on 3/28/2017.
 */

public class Constants {
    public static final class PAKISTANI_COMPANY{
        public static final class WARID {
            public static final String NAME = "WARID";
            public static final String DIALERCODE = "*123*";
            public static final String CARD_PATTERN = "^[\\d]{14}$";
            public static final String BALANCE_CHECK_CODE = "*100#";
            public static final String HELPLINE = "321";
        }

        public static final class JAZZ {
            public static final String NAME = "JAZZ";
            public static final String DIALERCODE = "*123*";
            public static final String CARD_PATTERN = "^[\\d]{14}$";
            public static final String BALANCE_CHECK_CODE = "*111#";
            public static final String HELPLINE = "111";
        }

        public static final class UFONE {
            public static final String NAME = "UFONE";
            public static final String DIALERCODE = "*123*";
            public static final String CARD_PATTERN = "^[\\d]{4} [\\d]{4} [\\d]{5} [\\d]{2}$";
            public static final String BALANCE_CHECK_CODE = "*124#";
            public static final String BALANCE_SHARE_PRE_CODE = "*828*";
            public static final String BALANCE_SHARE_POST_CODE = "*25#";
            public static final String HELPLINE = "333";
        }

        public static final class TELENOR {
            public static final String NAME = "TELENOR";
            public static final String DIALERCODE = "*555*";
            public static final String CARD_PATTERN = "^[\\d]{2} [\\d]{2} [\\d]{2} [\\d]{2} [\\d]{2} [\\d]{2} [\\d]{2}$";
            public static final String BALANCE_CHECK_CODE = "*444#";
            public static final String BALANCE_SHARE_PRE_CODE = "*1*1*";
            public static final String BALANCE_SHARE_POST_CODE = "*25#";
            public static final String HELPLINE = "345";
        }

        public static final class ZONG {
            public static final String NAME = "ZONG";
            public static final String DIALERCODE = "*101*";
            public static final String CARD_PATTERN = "^[\\d]{15}$";
            public static final String BALANCE_CHECK_CODE = "*222#";
            public static final String BALANCE_SHARE_SENDING_CODE = "999";
            public static final String BALANCE_SHARE_POST_CODE = ".25";
            public static final String HELPLINE = "310";
        }
    }

    public static final class SIMOPERATORNAME{
        public static final String SAWA_SIM_OPERATOR_NAME = "STC";
        public static final String ZAIN_SIM_OPERATOR_NAME = "Zain KSA";
        public static final String MOBILY_SIM_OPERATOR_NAME = "mobily";
        //public static final String FRIENDI_SIM_OPERATOR_NAME = "FRiENDi mobile";
        public static final String FRIENDI_SIM_OPERATOR_NAME = "Virgin Mobile";
        public static final String LEBARA_SIM_OPERATOR_NAME = "Lebara";
    }

    public static final class DIALERCODE{
        public static final String SAWA_DIALERCODE = "*155*";
        public static final String ZAIN_DIALERCODE = "*141*";
        public static final String MOBILY_DIALERCODE = "*1400*";
        public static final String FRIENDI_DIALERCODE = "*101*";
        public static final String LEBARA_DIALERCODE = "*111*";
    }

    public static final class MATCHPATTERN{
        public static final String FRIENDI_PLASTIC_CARD_MATCH_PATTERN = "([\\d]{4} [\\d]{3} [\\d]{4} [\\d]{3})";
    }

    public static final class CARDTYPE{
        //PAPER CARDTYPE
            //SAWA
                 public static final String SAWA_PAPER_CARD_TYPE = "SAWA_PAPER_CARD_TYPE";
            //ZAIN
                 public static final String ZAIN_PAPER_CARD_TYPE = "ZAIN_PAPER_CARD_TYPE";
            //MOBILY
                public static final String MOBILY_PAPER_CARD_TYPE = "MOBILY_PAPER_CARD_TYPE";
            //FRIENDI

        //PLASTIC CARDTYPE
            //SAWA
                public static final String SAWA_PLASTIC_CARD_TYPE = "SAWA_PLASTIC_CARD_TYPE";
            //ZAIN
                public static final String ZAIN_PLASTIC_CARD_TYPE = "ZAIN_PLASTIC_CARD_TYPE";
            //MOBILY
                public static final String MOBILY_PLASTIC_CARD_TYPE = "MOBILY_PLASTIC_CARD_TYPE";
            //FRIENDI
                public static final String FRIENDI_PLASTIC_CARD_TYPE = "FRIENDI_PLASTIC_CARD_TYPE";
            //LEBARA
                public static final String LEBARA_PLASTIC_CARD_TYPE = "LEBARA_PLASTIC_CARD_TYPE";
    }

    public static final class CARDPATTERN{
        //PAPER CARDPATTERN
            //SAWA
//        public static final String SAWA_PAPER_CARD_PATTERN = "^[\\d]{5} [\\d]{5} [\\d]{4}$";
        public static final String SAWA_PAPER_CARD_PATTERN = "^[\\d]{14}$";
            //ZAIN
//            public static final String ZAIN_PAPER_CARD_PATTERN = "^[\\d]{4} [\\d]{4} [\\d]{4} [\\d]{2}$";
              public static final String ZAIN_PAPER_CARD_PATTERN = "^[\\d]{5} [\\d]{4} [\\d]{5}$";
            //MOBILY
                public static final String MOBILY_PAPER_CARD_PATTERN = "^[\\d]{6} [\\d]{6} [\\d]{5}$";
            //FRIENDI

        //PLASTIC CARDPATTERN
            //SAWA
                public static final String SAWA_PLASTIC_CARD_PATTERN = "^[\\d]{7} [\\d]{7}$";
            //ZAIN
                public static final String ZAIN_PLASTIC_CARD_PATTERN = "^[\\d]{4} [\\d]{3} [\\d]{4} [\\d]{3}$";
            //MOBILY
           // public static final String MOBILY_PLASTIC_CARD_PATTERN = "^[\\d]{4} [\\d]{4} [\\d]{4} [\\d]{4} [\\d]{1}$";
        public static final String MOBILY_PLASTIC_CARD_PATTERN = "^[\\d]{3} [\\d]{4} [\\d]{3} [\\d]{4} [\\d]{3}$";
            //FRIENDI
                public static final String FRIENDI_PLASTIC_CARD_PATTERN = "^.*[\\d]{4} [\\d]{3} [\\d]{4} [\\d]{3}.*$";
            //LEBARA
                public static final String LEBARA_PLASTIC_CARD_PATTERN = "^[\\d]{14}$";
    }

}
