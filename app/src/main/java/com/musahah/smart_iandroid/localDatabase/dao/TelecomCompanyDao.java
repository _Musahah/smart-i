package com.musahah.smart_iandroid.localDatabase.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.musahah.smart_iandroid.localDatabase.models.Country;
import com.musahah.smart_iandroid.localDatabase.models.TelecomCompany;

import java.util.List;

@Dao
public interface TelecomCompanyDao {
    @Query("SELECT * FROM TelecomCompany")
    LiveData<List<TelecomCompany>> fetchAllTelecomCompanies();

    @Insert
    void insertTelecomCompany(TelecomCompany telecomCompany);

    @Insert
    void insertTelecomCompanies(List<TelecomCompany> telecomCompanies);

    @Query("DELETE FROM TelecomCompany")
    void deleteTelecomCompanies();

    @Query("SELECT COUNT(*) from TelecomCompany")
    LiveData<Integer> count();
}
