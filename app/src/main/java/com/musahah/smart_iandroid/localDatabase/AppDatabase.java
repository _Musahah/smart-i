package com.musahah.smart_iandroid.localDatabase;


import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.musahah.smart_iandroid.Application.App;
import com.musahah.smart_iandroid.Utils.AppConstants;
import com.musahah.smart_iandroid.localDatabase.dao.CountryDao;
import com.musahah.smart_iandroid.localDatabase.dao.TelecomCompanyDao;
import com.musahah.smart_iandroid.localDatabase.models.Country;
import com.musahah.smart_iandroid.localDatabase.models.TelecomCompany;


@Database(entities = {
        Country.class,
        TelecomCompany.class
},
        version = 1,
        exportSchema = false
)
public abstract class AppDatabase extends RoomDatabase {

    public abstract CountryDao countryAccess();
    public abstract TelecomCompanyDao telecomCompanyAccess();

}
