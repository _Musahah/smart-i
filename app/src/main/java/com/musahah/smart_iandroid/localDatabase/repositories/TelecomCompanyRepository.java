package com.musahah.smart_iandroid.localDatabase.repositories;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.musahah.smart_iandroid.Utils.AppConstants;
import com.musahah.smart_iandroid.localDatabase.AppDatabase;
import com.musahah.smart_iandroid.localDatabase.models.Country;
import com.musahah.smart_iandroid.localDatabase.models.TelecomCompany;

import java.util.List;

public class TelecomCompanyRepository {

    private static AppDatabase appDatabase;
    public TelecomCompanyRepository(Context context) {
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, AppConstants.DB_NAME).build();
    }

    public void insertTelecomCompany(final TelecomCompany telecomCompany) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDatabase.telecomCompanyAccess().insertTelecomCompany(telecomCompany);
                return null;
            }
        }.execute();
    }

    public void deleteTelecomCompanies() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDatabase.telecomCompanyAccess().deleteTelecomCompanies();
                return null;
            }
        }.execute();

    }

    public LiveData<Integer> count() {
        return appDatabase.telecomCompanyAccess().count();
    }

    public void insertTelecomCompanies(final List<TelecomCompany> telecomCompanies) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDatabase.telecomCompanyAccess().insertTelecomCompanies(telecomCompanies);
                return null;
            }
        }.execute();
    }

    public LiveData<List<TelecomCompany>> getTelecomCompanies() {
        return appDatabase.telecomCompanyAccess().fetchAllTelecomCompanies();
    }
}
