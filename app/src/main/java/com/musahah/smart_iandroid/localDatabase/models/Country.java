package com.musahah.smart_iandroid.localDatabase.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Country implements Parcelable {

    /**
     * title : Saudi Arabia
     * body :
     * field_calling_code : 966
     * field_currency_code : SAR
     * field_flag : /sites/default/files/2020-04/Saudi%20arabia%20flag.jpg
     * field_iso_code : SA
     * nid : 6
     * uuid : 95babb22-2ae5-4ff0-a96d-bdca85326df5
     */

    private String title;
    private String body;
    private String field_calling_code;
    private String field_currency_code;
    private String field_flag;
    private String field_iso_code;
    private String nid;

    @PrimaryKey
    @NonNull
    private String uuid;

    public Country() {
    }

    protected Country(Parcel in) {
        title = in.readString();
        body = in.readString();
        field_calling_code = in.readString();
        field_currency_code = in.readString();
        field_flag = in.readString();
        field_iso_code = in.readString();
        nid = in.readString();
        uuid = in.readString();
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getField_calling_code() {
        return field_calling_code;
    }

    public void setField_calling_code(String field_calling_code) {
        this.field_calling_code = field_calling_code;
    }

    public String getField_currency_code() {
        return field_currency_code;
    }

    public void setField_currency_code(String field_currency_code) {
        this.field_currency_code = field_currency_code;
    }

    public String getField_flag() {
        return field_flag;
    }

    public void setField_flag(String field_flag) {
        this.field_flag = field_flag;
    }

    public String getField_iso_code() {
        return field_iso_code;
    }

    public void setField_iso_code(String field_iso_code) {
        this.field_iso_code = field_iso_code;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(field_calling_code);
        parcel.writeString(field_currency_code);
        parcel.writeString(field_flag);
        parcel.writeString(field_iso_code);
        parcel.writeString(nid);
        parcel.writeString(uuid);
    }
}
