package com.musahah.smart_iandroid.localDatabase.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TelecomCompany implements Parcelable {

    /**
     * title : Ufone
     * body :
     * field_logo : /sites/default/files/2020-04/Ufone%20Pakistan%20company.png
     * field_which_country : Pakistan
     * nid : 7
     * uuid : 830cb1f9-a76d-4c3b-b9dc-42cd812f8684
     */

    private String title;
    private String body;
    private String field_logo;
    private String field_which_country;
    private String nid;

    @PrimaryKey
    @NonNull
    private String uuid;

    public TelecomCompany() {
    }

    protected TelecomCompany(Parcel in) {
        title = in.readString();
        body = in.readString();
        field_logo = in.readString();
        field_which_country = in.readString();
        nid = in.readString();
        uuid = in.readString();
    }

    public static final Creator<TelecomCompany> CREATOR = new Creator<TelecomCompany>() {
        @Override
        public TelecomCompany createFromParcel(Parcel in) {
            return new TelecomCompany(in);
        }

        @Override
        public TelecomCompany[] newArray(int size) {
            return new TelecomCompany[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getField_logo() {
        return field_logo;
    }

    public void setField_logo(String field_logo) {
        this.field_logo = field_logo;
    }

    public String getField_which_country() {
        return field_which_country;
    }

    public void setField_which_country(String field_which_country) {
        this.field_which_country = field_which_country;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(field_logo);
        parcel.writeString(field_which_country);
        parcel.writeString(nid);
        parcel.writeString(uuid);
    }
}
