package com.musahah.smart_iandroid.localDatabase.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.musahah.smart_iandroid.localDatabase.models.Country;

import java.util.List;

import retrofit2.http.DELETE;

@Dao
public interface CountryDao {
    @Query("SELECT * FROM Country")
    LiveData<List<Country>> fetchAllCountries();

    @Insert
    void insertCountry(Country country);

    @Insert
    void insertCountries(List<Country> countries);

    @Query("DELETE FROM Country")
    void deleteCountries();

    @Query("SELECT COUNT(*) from Country")
    LiveData<Integer> count();
}
