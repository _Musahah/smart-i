package com.musahah.smart_iandroid.localDatabase.repositories;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.musahah.smart_iandroid.Utils.AppConstants;
import com.musahah.smart_iandroid.localDatabase.AppDatabase;
import com.musahah.smart_iandroid.localDatabase.models.Country;

import java.util.List;

public class CountryRepository {

    private static AppDatabase appDatabase;
    public CountryRepository(Context context) {
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, AppConstants.DB_NAME).build();
    }

    public void insertCountry(final Country country) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDatabase.countryAccess().insertCountry(country);
                return null;
            }
        }.execute();
    }

    public void deleteCountries() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDatabase.countryAccess().deleteCountries();
                return null;
            }
        }.execute();

    }

    public LiveData<Integer> count() {
        return appDatabase.countryAccess().count();
    }

    public void insertCountries(final List<Country> countries) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDatabase.countryAccess().insertCountries(countries);
                return null;
            }
        }.execute();
    }

    public LiveData<List<Country>> getCountries() {
        return appDatabase.countryAccess().fetchAllCountries();
    }
}
