package com.musahah.smart_iandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.musahah.smart_iandroid.MyLib.MyLib;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.localDatabase.models.Country;

import java.util.List;

import static com.musahah.smart_iandroid.Utils.AppConstants.BASE_URL;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

    private final List<Country> mValues;
    private Context context;
    OnShareClickedListener mCallback;


    public CountryAdapter(Context context, List<Country> countryList) {
        this.context = context;
        this.mValues = countryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_text_rv_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Country current = mValues.get(position);

        // Set image
        MyLib.GLIDE_IMAGE.setRoundedImageGlide(context, BASE_URL+current.getField_flag(), R.drawable.app_logo, holder.countryFlag);

        holder.countryName.setText(current.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.ShareClicked(current.getNid()+" Nid");
            }
        });
    }

    public void setOnShareClickedListener(OnShareClickedListener mCallback) {
        this.mCallback = mCallback;
    }

    public interface OnShareClickedListener {
        public void ShareClicked(String url);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView countryFlag;
        public TextView countryName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            countryFlag = itemView.findViewById(R.id.countryflag_iv);
            countryName = itemView.findViewById(R.id.countryname_tv);
        }
    }
}
