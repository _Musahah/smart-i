package com.musahah.smart_iandroid.network.services;

import com.musahah.smart_iandroid.localDatabase.models.Country;
import com.musahah.smart_iandroid.localDatabase.models.TelecomCompany;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Services {
    @GET("/api/countries")
    Call<List<Country>> getCountries();

    @GET("/api/companiesbycountry/{countryNid}")
    Call<List<TelecomCompany>> getTelecomCompanies(@Path("countryNid") String countryNid);
}
