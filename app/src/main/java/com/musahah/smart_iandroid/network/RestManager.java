package com.musahah.smart_iandroid.network;


import com.musahah.smart_iandroid.Utils.SharedPrefUtil;
import com.musahah.smart_iandroid.network.services.Services;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.musahah.smart_iandroid.Utils.AppConstants.BASE_URL;

public class RestManager {
    private GsonConverterFactory gsonFactory = GsonConverterFactory.create(); // Initializing Gson parser object
    private Retrofit retrofit;
    private OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static RestManager mRestManagerInstance;

    private Services mServices;

    // Parameterize Constructor
    private RestManager(boolean useAuth){
//        if(useAuth)
//            initServiceAuth(); // Make service auth header
//        else
            initService(); // Make service without auth header
    }

    // synchronized is used if multiple methods calling the same instance at the same time
    public static synchronized RestManager getRestManagerInstance(boolean useAuth) {
        mRestManagerInstance = new RestManager(useAuth);
        return mRestManagerInstance;
    }

    // Initializing Retrofit object
    private void initService(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonFactory)
                .build();
    }

    // Initializing OkHttp object with authorization header and make retrofit object
    private void initServiceAuth(){
        // Set call timeout
        httpClient
                .connectTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Origin", "http://localhost")
                        .method(original.method(), original.body());

                if(SharedPrefUtil.getInstance().getToken() != null && !SharedPrefUtil.getInstance().getToken().isEmpty()){
                    requestBuilder.header("token", SharedPrefUtil.getInstance().getToken());
                }

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        retrofit = new Retrofit.Builder()
                .baseUrl(SharedPrefUtil.getInstance().getBaseApiUrl())
                .addConverterFactory(gsonFactory)
                .client(httpClient.build())
                .build();
    }

    // Getting Language services
    public Services getServices(){
        if(mServices == null){
            mServices = retrofit.create(Services.class);
        }

        return mServices;
    }
}
