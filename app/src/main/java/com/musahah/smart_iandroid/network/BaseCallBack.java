package com.musahah.smart_iandroid.network;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.musahah.smart_iandroid.Application.BaseActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseCallBack<T> extends BaseActivity implements Callback<T> {
    private Wrapper<T> wrapper;
    private Activity activity;
    private String onFailureMessage = "";

    public BaseCallBack(Wrapper<T> wrapper, boolean showLoader, Activity activity) {
//        if (showLoader)
//            showLoader();

        this.wrapper = wrapper;
        this.activity = activity;
    }

    public void onFailure(Call<T> call, Throwable t) {
       // hideLoader();
        //feedbackMessage(activity, "Internet connection failed", NotificationType.ERROR);

        wrapper.onResult(t, null);
    }

    public void onResponse(Call<T> call, Response<T> response) {
        //hideLoader();

        Log.d("URL", response.raw().request().url().toString());
        Log.d("Response", new Gson().toJson(response));

//        if (response.code() == 500) {
//            //feedbackMessage(activity, "Internal Server error", NotificationType.ERROR);
//        } else if (response.code() == 401) {
//            //feedbackMessage(activity, "Unauthorized", NotificationType.ERROR);
//        } else if (response.code() == 404) {
//           // feedbackMessage(activity, "Not found Or Invalid request", NotificationType.ERROR);
//        } else if (response.code() == 200) {
//            Log.d("URL",response.raw().request().url().toString());
//
//            if (response.isSuccessful()) {
//                if (response.body() != null) {
//
//                } else {
//                  //  feedbackMessage(activity, "Null response", NotificationType.ERROR);
//                }
//            }
//        }

        wrapper.onResult(null, response);
    }

    public interface Wrapper<T> {
        void onResult(Throwable t, Response<T> response);
    }
}
