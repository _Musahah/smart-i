package com.musahah.smart_iandroid.MyLib;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.transition.TransitionInflater;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.core.util.Pair;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.musahah.smart_iandroid.Activities.CountryActivity;
import com.musahah.smart_iandroid.Application.App;
import com.musahah.smart_iandroid.Application.BaseActivity;
import com.musahah.smart_iandroid.R;
import com.pd.chocobar.ChocoBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.LandingAnimator;
import okhttp3.ResponseBody;


public class MyLib extends BaseActivity {
    public static class Variables {
        // Recycler view animation duration
        public static int RECYCLERVIEW_ANIMATION_DURATION = 500;

        // Location type
        enum LocationType {
            CITY(1),
            PROVINCE(2),
            COUNTRY(3);

            private int value;
            private static Map map = new HashMap<>();

            private LocationType(int value) {
                this.value = value;
            }

            static {
                for (LocationType pageType : LocationType.values()) {
                    map.put(pageType.value, pageType);
                }
            }

            public static LocationType valueOf(int pageType) {
                return (LocationType) map.get(pageType);
            }

            public int getValue() {
                return value;
            }
        }
    }

    public static class Screenshot{

        public static Bitmap takeScreenshot(View v){
            v.setDrawingCacheEnabled(true);
            return v.getDrawingCache();
        }

        public static boolean saveBitmap(Bitmap bitmap) {
            File imagePath = new File(Environment.getExternalStorageDirectory() + "/screenshot-"+ Calendar.getInstance().getTime() +".png");
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(imagePath);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();

                return true;
            } catch (FileNotFoundException e) {
                Log.e("GREC", e.getMessage(), e);
            } catch (IOException e) {
                Log.e("GREC", e.getMessage(), e);
            }

            return false;
        }
    }

    public static final class LOCATION_FUNCTION {
        private static LocationManager locationManager;
        private static LocationListener locationListener;
        private static Geocoder geocoder;
        private static List<Address> addresses;

        // Get GPS status
        public static Boolean getGpsStatus(Activity activity) {
            locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);

            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }

        // Open Map with given locaiton
        public static void openMap(Activity activity, double bookingLocationLat, double bookingLocationLon){
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("geo:"+bookingLocationLat+","+bookingLocationLon+"?q="+bookingLocationLat+","+bookingLocationLon));
            activity.startActivity(intent);
        }

        // Go to Enable GPS
        public static void accessGps(final Activity activity) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage("Your GPS seems to be disabled, Please enable it!")
                    .setCancelable(false)
                    .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            activity.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//                        dialog.cancel();
//                    }
//                });
            final AlertDialog alert = builder.create();
            alert.show();
        }

        // Get current location
        public static Location getCurrentLocation(final Context activity) {

            final Context context = activity;
            // flag for GPS status
            boolean isGPSEnabled = false;

            // flag for network status
            boolean isNetworkEnabled = false;

            // flag for GPS status
            boolean canGetLocation = false;

            Location location = null; // location
            double latitude; // latitude
            double longitude; // longitude

            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };

            try {
                //locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);

                // getting GPS status
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                // getting network status
                isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (!isGPSEnabled && !isNetworkEnabled) {
                    // no network provider is enabled
                } else {
                    // First get location from Network Provider
                    if (isNetworkEnabled) {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 1, locationListener);
                        Log.d("Network", "Network");
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, locationListener);
                            Log.d("GPS Enabled", "GPS Enabled");
                            if (locationManager != null) {
                                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return location;
        }

        // Get location city through longitude and latitude
        // Returns Area/City of location provided latitude and longitude, it also required context
        public static String getFromLocation(double latitude, double longitude, Context context, Variables.LocationType locationType){
            addresses = Collections.emptyList();
            geocoder = new Geocoder(context, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(addresses.isEmpty())
                return null;

            else{
                String area = addresses.get(0).getLocality();
                String province = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();

                switch (locationType.value){
                    case 1: // for city
                        return area;
                    case 2: // for city province
                        return province;
                    case 3: // country name
                        return country;
                }
            }
            // if reached till here, then something went wrong;
            return null;
        }
    }

    public static class System {
        // Get Device id
        public static String getDeviceId(Context context) {
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        public static void setClipboard(Context context, String text) {
            if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(text);
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
                clipboard.setPrimaryClip(clip);
            }
        }

        // Hide elements
        public static void hideElements(List<View> view){
            for(int i=0; i<view.size(); i++){
                view.get(i).setVisibility(View.GONE);
            }
        }

        // Hide elements
        public static void showElements(List<View> view){
            for(int i=0; i<view.size(); i++){
                view.get(i).setVisibility(View.VISIBLE);
            }
        }

        // Check if sim is inserted or not
        public static boolean isSimSupport(Context context)
        {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
            return !(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT);
        }

        // Check internet connection
        public static boolean isNetworkAvailable(Context context) {
            boolean outcome = false;

            if (context != null) {
                ConnectivityManager cm = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo[] networkInfos = cm.getAllNetworkInfo();

                for (NetworkInfo tempNetworkInfo : networkInfos) {


                    /**
                     * Can also check if the user is in roaming
                     */
                    if (tempNetworkInfo.isConnected()) {
                        outcome = true;
                        break;
                    }
                }
            }

            return outcome;
        }

        // Custom method to get screen width in dp/dip using Context object
        public static int getScreenWidthInDPs(Context context){
        /*
            DisplayMetrics
                A structure describing general information about a display,
                such as its size, density, and font scaling.
        */
            DisplayMetrics dm = new DisplayMetrics();

        /*
            WindowManager
                The interface that apps use to talk to the window manager.
                Use Context.getSystemService(Context.WINDOW_SERVICE) to get one of these.
        */

        /*
            public abstract Object getSystemService (String name)
                Return the handle to a system-level service by name. The class of the returned
                object varies by the requested name. Currently available names are:

                WINDOW_SERVICE ("window")
                    The top-level window manager in which you can place custom windows.
                    The returned object is a WindowManager.
        */

        /*
            public abstract Display getDefaultDisplay ()

                Returns the Display upon which this WindowManager instance will create new windows.

                Returns
                The display that this window manager is managing.
        */

        /*
            public void getMetrics (DisplayMetrics outMetrics)
                Gets display metrics that describe the size and density of this display.
                The size is adjusted based on the current rotation of the display.

                Parameters
                outMetrics A DisplayMetrics object to receive the metrics.
        */
            WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(dm);
            int widthInDP = Math.round(dm.widthPixels / dm.density);
            return widthInDP;
        }

        // Custom method to get screen height in dp/dip using Context object
        public static int getScreenHeightInDPs(Context context){
            DisplayMetrics dm = new DisplayMetrics();
            WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(dm);
        /*
            In this example code we converted the float value
            to nearest whole integer number. But, you can get the actual height in dp
            by removing the Math.round method. Then, it will return a float value, you should
            also make the necessary changes.
        */

        /*
            public int heightPixels
                The absolute height of the display in pixels.

            public float density
             The logical density of the display.
        */
            int heightInDP = Math.round(dm.heightPixels / dm.density);
            return heightInDP - 55;
        }
    }

    public static class Dimension {
        /**
         * This method converts dp unit to equivalent pixels, depending on device density.
         *
         * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
         * @return A float value to represent px equivalent to dp depending on device density
         */
        public static float convertDpToPixel(float dp){
            return dp * ((float) App.getApplicaionInstance().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        }

        /**
         * This method converts device specific pixels to density independent pixels.
         *
         * @param px A value in px (pixels) unit. Which we need to convert into db
         * @return A float value to represent dp equivalent to px value
         */
        public static float convertPixelsToDp(float px){
            return px / ((float) App.getApplicaionInstance().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        }
    }

    public static class ActivityNavigation{
        // Navigate from one activity to another activity
        public static void navigateTo(Activity activity, Class<?> aClass) {
            Intent intent = new Intent(activity, aClass);
            activity.startActivity(intent);
        }

        // Navigate from one activity to another activity and send data
        public static void setParcelableObjectAndNavigateToActivity(Activity activity, Class<?> aClass, String key, Parcelable parcelable){
            Intent intent = new Intent(activity, aClass);

            Bundle bundle = new Bundle();
            bundle.putParcelable(key, parcelable); // Setting object as given key

            intent.putExtras(bundle);
            activity.startActivity(intent);
        }

        // Navigate from one activity to another activity and send data
        public static void setListOfParcelableObjectAndNavigateToActivity(Activity activity, Class<?> aClass, String key, ArrayList<? extends Parcelable> parcelable){
            Intent intent = new Intent(activity, aClass);

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(key, parcelable); // Setting object as given key

            intent.putExtras(bundle);
            activity.startActivity(intent);
        }

        public static void setListOfMultipleParcelableObjectAndNavigateToActivity(Activity activity, Class<?> aClass, ArrayList<String> keys, ArrayList<ArrayList<? extends Parcelable>> parcelables){
            Intent intent = new Intent(activity, aClass);

            Bundle bundle = new Bundle();
            for(int i=0; i<keys.size(); i++){
                bundle.putParcelableArrayList(keys.get(i), parcelables.get(i)); // Setting object as given key
            }

            intent.putExtras(bundle);
            activity.startActivity(intent);
        }

        // Refresh to current page
        public static void goToCurrentScreen(Context context, Class<?> Class){
            // Refresh locale to update view
            context.startActivity(new Intent(context, Class));
        }
    }

    public static class FragmentNavigation{
        /* Replaces Fragment on  container with given fragment and add previous fragment on backStack,
         so when back pressed it will go on previous fragment */
        public static void replaceFragment(FragmentActivity activity, int containerId, Fragment fragment, boolean addToBackStack) {
            FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

            if (addToBackStack) {
                transaction.addToBackStack(null);
            }

            transaction.replace(containerId, fragment)
                    .commit();
        }


        /* Get Fragment to replaces, Key of Object data, and object as parcelable */
        public static Fragment setParcelableObjectToSendFragment(Fragment fragment, String key, Parcelable parcelable) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(key, parcelable); // Setting object as given key
            fragment.setArguments(bundle);

            return fragment;  // Returns fragment with object argument set as passed Key
        }

        /* Get Fragment to replaces, Key of Object data, and object as parcelable */
        public static Fragment setListOfParcelableObjectToSendFragment(Fragment fragment, String key, ArrayList<? extends Parcelable> parcelable) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(key, parcelable); // Setting object as given key
            fragment.setArguments(bundle);

            return fragment;  // Returns fragment with object argument set as passed Key
        }

        // Send multiple list objects
        public static Fragment setListOfMultipleParcelableObjectToSendFragment(Fragment fragment, ArrayList<String> keys, ArrayList<ArrayList<? extends Parcelable>> parcelables){
            Bundle bundle = new Bundle();
            for(int i=0; i<keys.size(); i++){
                bundle.putParcelableArrayList(keys.get(i), parcelables.get(i)); // Setting object as given key
                fragment.setArguments(bundle);
            }

            return fragment;  // Returns fragment with object argument set as passed Key
        }

    }

    public static class Layout{
        // Set layout direction by locale
        public static void setLayoutDirection(String lang, Context context){
            Configuration configuration = context.getResources().getConfiguration();
            configuration.setLayoutDirection(new Locale(lang));
            context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
        }
    }

    public static final class GLIDE_IMAGE {
        // set image url using glide
        public static void setImageGlide(Context context, String urlImage, int errorImage, ImageView intoView) {
            // Set image
            Glide.with(context)
                    .load(urlImage)
                    .error(errorImage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(errorImage)
                    .into(intoView);
        }

        // set rounded image url using glide
        public static void setRoundedImageGlide(final Context context, String urlImage, int errorImage, final ImageView intoView) {
            // Set image
            Glide
                    .with(context)
                    .load(urlImage)
                    .asBitmap()
                    .placeholder(errorImage)
                    .error(errorImage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(new BitmapImageViewTarget(intoView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            intoView.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        }

        // set overlay on image url using glide
        public static void setOverLayOnImageGlide(final Context context, String urlImage, int errorImage, final ImageView intoView, final int overlayImage) {
            // Set image
            Glide
                    .with(context)
                    .load(urlImage)
                    .asBitmap()
                    .placeholder(errorImage)
                    .error(errorImage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            resource = BitmapUtils.applyOverlay(context, resource, overlayImage);
                            intoView.setImageBitmap(resource);
                        }
                    });
        }
    }

    public static class Drawable{
        // Get drawable int
        public static int getDrawable(Context context, String drawableName) {
            return context.getResources().getIdentifier(drawableName, "drawable", context.getPackageName());
        }
    }

    public static class Validation{
        /**
         * method is used for checking valid email id format.
         *
         * @param email
         * @return boolean true for valid false for invalid
         */
        public static boolean isEmailValid(String email) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        }

        public static boolean isValidByRegex(String input, String regex){
            if (input.matches(regex))
                return true;
            else
                return false;
        }
    }

    public static class LayoutManager{
        // Returns LinearLayoutManager
        public static LinearLayoutManager getLinearLayoutManager(Context context) {
            return new LinearLayoutManager(context);
        }

        // Returns GridLayoutManager
        public static GridLayoutManager getGridLayoutManager(Context context, int numberOfColums) {
            return new GridLayoutManager(context, numberOfColums);
        }

        // Returns Horizontal LinearLayoutManager
        public static LinearLayoutManager getHorizontalLinearLayoutManager(Context context) {
            return new LinearLayoutManager(context, androidx.recyclerview.widget.RecyclerView.HORIZONTAL, false);
        }

    }

    public static class RecyclerView{
        // Setting layoutManager and Animated Adapter to RecyclerView
        public static void setRecyclerViewAndAdapter(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.Adapter adapter, Activity activity, int layoutManagerType, @Nullable int columns){
            //RecyclerView all items enter animation
            LandingAnimator landingAnimator = new LandingAnimator(new OvershootInterpolator(1.2f));

            //RecyclerView Adapter scrolling animation
            ScaleInAnimationAdapter animatedAdapter = new ScaleInAnimationAdapter(adapter);
            animatedAdapter.setFirstOnly(false); // Disable the first scroll mode.

            //Setting RecyclerView
            recyclerView.setHasFixedSize(true);
            recyclerView.setRecycledViewPool(new androidx.recyclerview.widget.RecyclerView.RecycledViewPool());
            switch(layoutManagerType){
                case 1: // To set linear layout manager
                    //recyclerView.setItemAnimator(landingAnimator);
                    //recyclerView.getItemAnimator().setAddDuration(ShujatLib.VARIABLE.RECYCLERVIEW_ANIMATION_DURATION);
                    recyclerView.setLayoutManager(LayoutManager.getLinearLayoutManager(activity)); // Set linear layout manager
                    break;
                case 2: // To set Grid layout manager
                    recyclerView.setItemAnimator(landingAnimator); // set landing animator
                    recyclerView.getItemAnimator().setAddDuration(Variables.RECYCLERVIEW_ANIMATION_DURATION);
                    recyclerView.setLayoutManager(LayoutManager.getGridLayoutManager(activity, columns));
                    break;
                case 3: // To set horizontal linear layout manager
                    //recyclerView.setItemAnimator(landingAnimator);
                    //recyclerView.getItemAnimator().setAddDuration(ShujatLib.VARIABLE.RECYCLERVIEW_ANIMATION_DURATION);
                    recyclerView.setLayoutManager(LayoutManager.getHorizontalLinearLayoutManager(activity)); // Set linear layout manager
                    break;
            }

            //recyclerView.setAdapter(animatedAdapter);
            recyclerView.setAdapter(adapter);
        }
    }

    public static final class ANIMATION{
        private static ActivityOptionsCompat options;
        private static Intent intent;
        private static Pair[] stringPair;
        private static Animation anim;

        // Shared animation between Activities lollipop and above
        public static void setMultiSharedAnimActivitiesPostLollipop(Activity activity, Class<?> aClass, ArrayList<View> views){
            intent = new Intent(activity, aClass);
            stringPair = new Pair[views.size()];

            for(int i=0; i<views.size(); i++){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    stringPair[i] = Pair.create(views.get(i), views.get(i).getTransitionName());
                }
            }

            options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, stringPair);
            activity.startActivity(intent, options.toBundle());
        }

        // Shared animation between Activities lollipop and above
        public static void setMultiSharedElementsWithParcellablePostLollipop(Activity activity, Class<?> aClass, String key, Parcelable parcelable, ArrayList<View> views){
            intent = new Intent(activity, aClass);
            stringPair = new Pair[views.size()];

            for(int i=0; i<views.size(); i++){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    stringPair[i] = Pair.create(views.get(i), views.get(i).getTransitionName());
                }
            }

            Bundle bundle = new Bundle();
            bundle.putParcelable(key, parcelable); // Setting object as given key

            intent.putExtras(bundle);

            options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, stringPair);
            activity.startActivity(intent, options.toBundle());
        }

        // Shared single element animation between Activities lollipop and above
        public static void singleSharedElementTransitionPostLollipop(Activity activity, Class<?> aClass,  View view){
            intent = new Intent(activity, aClass);
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, view, ViewCompat.getTransitionName(view));

            activity.startActivity(intent, options.toBundle());
        }

        // Navigate from one activity to another activity and send data with transition
        public static void setParcelableObjectAndTransitionWithSingleSharedElementToActivity(Activity activity, Class<?> aClass, String key, Parcelable parcelable, View view){
            intent = new Intent(activity, aClass);

            Bundle bundle = new Bundle();
            bundle.putParcelable(key, parcelable); // Setting object as given key

            options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, view, ViewCompat.getTransitionName(view));

            intent.putExtras(bundle);
            activity.startActivity(intent, options.toBundle());
        }

        // Received shared element animation lollipop and above
        public static void recieveMultiSharedAnimActivitiesPostLollipop(Activity activity){
           // activity.getWindow().setSharedElementEnterTransition(TransitionInflater.from(activity).inflateTransition(R.transition.shared_element_transition_settings));
        }

        // Shared animation between Activities below lollipop
        public static void setSingleSharedAnimActivitiesPreLollipop(Activity activity, Class<?> aClass, View view){
            intent = new Intent(activity, aClass);
            //ActivityTransitionLauncher.with(activity).from(view).launch(intent);
        }

        // Received shared element animation below lollipop
        public static void receieveSingleSharedAnimActivitiesPreLollipop(Activity activity, View view, Bundle savedInstanceState){
            //ActivityTransition.with(activity.getIntent()).to(view).start(savedInstanceState);
        }

        // Alpha animation
        public static void alphaAnimation(Activity activity, View view, int duration){
            anim = AnimationUtils.loadAnimation(activity, R.anim.anim_alpha);
            anim.reset();
            anim.setDuration(duration);
            view.clearAnimation();
            view.startAnimation(anim);
        }

        // SlideInUp animation
        public static void slideInUpAnimation(Activity activity, View view, int duration){
            anim = AnimationUtils.loadAnimation(activity, R.anim.anim_slide_in_up);
            anim.reset();
            anim.setDuration(duration);
            view.clearAnimation();
            view.startAnimation(anim);
        }

        public static void slideInRightAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_slide_in_right);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void slideInLeftAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_slide_in_left);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void slideOutRightAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_slide_out_right);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void slideOutLeftAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_slide_out_left);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void slideUpAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.slide_up);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void slideDownAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_slide_in_down);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void slideOutUpAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_slide_out_up);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void fadeInAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.fadein);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void fadeOutAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_fade_out);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void slideInUpAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_slide_in_up);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        public static void slideOutDownAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.anim_slide_out_down);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }

        // Animation for scale up for a view
        public static void scaleUpAnimation(View view, int duration){
            Animation titleAnimation = AnimationUtils.loadAnimation(App.getApplicaionInstance(), R.anim.scale_up);
            titleAnimation.setDuration(duration);
            view.startAnimation(titleAnimation);
        }
    }

    public static final class Feedback {
        // show Snack for certain milliseconds of time using ChocoBar library
        public static void showSnackForCertainTime(Activity activity, String msg, int durationInMilliseconds) {
            ChocoBar.builder().setActivity(activity)
                    .setActionText("Ok")
                    .centerText()
                    .setTextColor(Color.parseColor("#E4EEF8"))
                    .setActionTextColor(Color.parseColor("#FF7C00"))
                    .setActionTextTypefaceStyle(Typeface.BOLD)
                    .setText(msg)
                    .setDuration(durationInMilliseconds)
                    .build()
                    .show();
        }

        // show Snack for infinite time using ChocoBar library
        public static void showSnackForInfiniteTime(Activity activity, String msg) {
            ChocoBar.builder().setActivity(activity)
                    .setActionText("Ok")
                    .centerText()
                    .setTextColor(Color.parseColor("#E4EEF8"))
                    .setActionTextColor(Color.parseColor("#FF7C00"))
                    .setActionTextTypefaceStyle(Typeface.BOLD)
                    .setText(msg)
                    .setDuration(ChocoBar.LENGTH_INDEFINITE)
                    .build()
                    .show();
        }

    }

    public static class BitmapUtils {

        public static Bitmap applyOverlay(Context context, Bitmap sourceImage, int overlayDrawableResourceId){
            Bitmap bitmap = null;
            try{
                int width = sourceImage.getWidth();
                int height = sourceImage.getHeight();
                Resources r = context.getResources();

                android.graphics.drawable.Drawable imageAsDrawable =  new BitmapDrawable(r, sourceImage);
                android.graphics.drawable.Drawable[] layers = new android.graphics.drawable.Drawable[2];

                layers[0] = imageAsDrawable;
                layers[1] = new BitmapDrawable(r, BitmapUtils.decodeSampledBitmapFromResource(r, overlayDrawableResourceId, width, height));
                LayerDrawable layerDrawable = new LayerDrawable(layers);
                bitmap = BitmapUtils.drawableToBitmap(layerDrawable);
            }catch (Exception ex){}
            return bitmap;
        }

        public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                             int reqWidth, int reqHeight) {

            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(res, resId, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeResource(res, resId, options);
        }

        public static int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }

        public static Bitmap drawableToBitmap (android.graphics.drawable.Drawable drawable) {
            Bitmap bitmap = null;

            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                if(bitmapDrawable.getBitmap() != null) {
                    return bitmapDrawable.getBitmap();
                }
            }

            if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        }
    }

    public static final class StringUtils{
        // Split give string into on certain given character
        public static String[] split(String s, String splitChar){
            return s.split(splitChar);
        }

        // Remove first and last character from string
        public static String removeFirstLastChar(String s){
            return s.substring(1, s.length() - 1);
        }

        // replace all english numeric digit to arabic numeric digit in a string
        public static String replaceEnglishNumericWithArabic(String englishString) {
            return englishString
                    .replace("1","١")
                    .replace("2","٢")
                    .replace("3","٣")
                    .replace("4","٤")
                    .replace("5","٥")
                    .replace("6","٦")
                    .replace("7","٧")
                    .replace("8","٨")
                    .replace("9","٩")
                    .replace("0","٠");
        }
    }

    public static final class Retrofit{
        public static String showResponseError(String responseErrorBody){
            // Response not successful parse error and show
            // Remove quotes from msg and show
            String errorMsg = StringUtils.removeFirstLastChar( responseErrorBody );
            return errorMsg;
        }

        // show error response message Json object value
        public static String showErrorResponseMessageByValue(ResponseBody responseErrorBody) {
            JSONObject jObjError = null;
            try {
                jObjError = new JSONObject(responseErrorBody.string());
                String tempError = jObjError.getString("error");
                jObjError = new JSONObject(tempError);
                return jObjError.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
