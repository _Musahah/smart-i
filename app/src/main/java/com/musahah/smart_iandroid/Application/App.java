package com.musahah.smart_iandroid.Application;

import android.app.Application;

import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.Utils.AppConstants;
import com.musahah.smart_iandroid.Utils.SharedPrefUtil;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends Application {
    public static App mAppInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(AppConstants.TYPEFACE_PATH) // You can change font path by going on AppConstants file under Utils
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

//        // Setting initial first time configuration settings
//        if(SharedPrefUtil.getInstance().getFirstRun()){
//            // setting firstRun to false
//            SharedPrefUtil.getInstance().setFirstRun(false);
//
//            // setting values
//            SharedPrefUtil.getInstance().setBaseApiUrl(AppConstants.BASE_URL);
//
//            // setting device id in pref
//            SharedPrefUtil.getInstance().setDeviceId(MyLib.System.getDeviceId(App.getApplicaionInstance()));
//        }
    }

    public App(){
        mAppInstance = this;
    }

    public static App getApplicaionInstance(){
        return mAppInstance;
    }
}
