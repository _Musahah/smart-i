/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.musahah.smart_iandroid.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bitvale.switcher.SwitcherX;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.text.TextRecognizer;
import com.musahah.smart_iandroid.MyLib.MyLib;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.Utils.SharedPrefUtil;
import com.musahah.smart_iandroid.helper.Constants;
import com.musahah.smart_iandroid.helper.OcrDetectorProcessor;
import com.musahah.smart_iandroid.helper.OcrGraphic;
import com.musahah.smart_iandroid.ui.camera.CameraSource;
import com.musahah.smart_iandroid.ui.camera.CameraSourcePreview;
import com.musahah.smart_iandroid.ui.camera.GraphicOverlay;

import java.io.IOException;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

/**
 * Activity for the Ocr Detecting app.  This app detects text and displays the value with the
 * rear facing camera. During detection overlay graphics are drawn to indicate the position,
 * size, and contents of each TextBlock.
 */
public class OcrCaptureFragment extends Fragment {
    private static final String TAG = "OcrCaptureActivity";

    // Intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // Permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    public static final int REQUEST_PHONE_CALL = 1;

    // Constants used to pass extra data in the intent
    public static final String AutoFocus = "AutoFocus";
    public static final String UseFlash = "UseFlash";
    public static final String TextBlockObject = "String";

    public String ID = "";
    public String SimOperatorName = "";
    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<OcrGraphic> mGraphicOverlay;

    // Helper objects for detecting taps and pinches.
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;

    // A TextToSpeech engine for speaking a String value.
    private TextToSpeech tts;
    private ImageView more;
    private Constants constants;

    boolean useFlash = false;
    private TextView detecting, instruction_text;
    View bar;


    private LinearLayout country_section, pak_telecom_company_section, saudia_telecom_company_section, ocrcapture_settings_section;
    private ImageView country_saudia, country_pakistan;
    private ImageView pak_company_warid, pak_company_ufone, pak_company_telenor, pak_company_jazz, pak_company_zong;
    private static final int PERMISSION_REQUEST_CODE = 200;
    // For torch light effect
    private ImageView torch;
    boolean autoFocus = true;

    /**
     * Initializes the UI and creates the detector pipeline.
     */
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        if (bundle == null) {
            Bundle extras = getActivity().getIntent().getExtras();
            if (extras == null) {
                SimOperatorName = null;
            } else {
                SimOperatorName = extras.getString("SimOperatorName");
            }
        } else {
            SimOperatorName = (String) bundle.getSerializable("SimOperatorName");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ocr_capture, container, false);

        detecting = view.findViewById(R.id.detectingvoucher);
        mPreview = view.findViewById(R.id.preview);
        mGraphicOverlay = view.findViewById(R.id.graphicOverlay);
        torch = view.findViewById(R.id.ocrcapture_imageview_torch);
        country_section = view.findViewById(R.id.country_section);
        pak_telecom_company_section = view.findViewById(R.id.pak_telecom_company_section);
        saudia_telecom_company_section = view.findViewById(R.id.saudia_telecom_company_section);
        country_saudia = view.findViewById(R.id.country_saudia);
        bar = view.findViewById(R.id.bar);
        instruction_text = view.findViewById(R.id.instruction_text);

        pak_company_warid = view.findViewById(R.id.pak_company_warid);
        pak_company_ufone = view.findViewById(R.id.pak_company_ufone);
        pak_company_telenor = view.findViewById(R.id.pak_company_telenor);
        pak_company_jazz = view.findViewById(R.id.pak_company_jazz);
        pak_company_zong = view.findViewById(R.id.pak_company_zong);

        constants = new Constants();

        switch (SimOperatorName) {
            case Constants.SIMOPERATORNAME.SAWA_SIM_OPERATOR_NAME:
                detecting.setText(getResources().getString(R.string.searching) + " " + getResources().getString(R.string.sawa) + " " + getResources().getString(R.string.voucher_number));
                break;
            case Constants.SIMOPERATORNAME.ZAIN_SIM_OPERATOR_NAME:
                detecting.setText(getResources().getString(R.string.searching) + " " + getResources().getString(R.string.zain) + " " + getResources().getString(R.string.voucher_number));
                break;
            case Constants.SIMOPERATORNAME.MOBILY_SIM_OPERATOR_NAME:
                detecting.setText(getResources().getString(R.string.searching) + " " + getResources().getString(R.string.mobily) + " " + getResources().getString(R.string.voucher_number));
                break;
            case Constants.SIMOPERATORNAME.FRIENDI_SIM_OPERATOR_NAME:
                detecting.setText(getResources().getString(R.string.searching) + " " + getResources().getString(R.string.friendi) + " " + getResources().getString(R.string.voucher_number));
                break;
            case Constants.SIMOPERATORNAME.LEBARA_SIM_OPERATOR_NAME:
                detecting.setText(getResources().getString(R.string.searching) + " " + getResources().getString(R.string.lebara) + " " + getResources().getString(R.string.voucher_number));
                break;
        }

        setPakCompaniesOnClick();

        // Set good defaults for capturing text.


//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
//        }

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        // int rc = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (checkPermission()) {
            createCameraSource(autoFocus, useFlash);
            torch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    useFlash = !useFlash;

                    if (useFlash) {
                        turnOnFlash();
                    } else {
                        turnOffFlash();
                    }
                }
            });
        } else {
            requestPermission();
        }

        gestureDetector = new GestureDetector(getActivity(), new CaptureGestureListener());
        scaleGestureDetector = new ScaleGestureDetector(getActivity(), new ScaleListener());

        setSelectedTelecomCompany();

        return view;
    }

    private void setSelectedTelecomCompany() {
        if(SharedPrefUtil.getInstance().getTelecomeCompanySelected() != null) {
            setScanAnimation();

            switch (SharedPrefUtil.getInstance().getTelecomeCompanySelected()) {
                case Constants.PAKISTANI_COMPANY.WARID.NAME:
                    pak_company_warid.performClick();
                    break;
                case Constants.PAKISTANI_COMPANY.UFONE.NAME:
                    pak_company_ufone.performClick();
                    break;
                case Constants.PAKISTANI_COMPANY.TELENOR.NAME:
                    pak_company_telenor.performClick();
                    break;
                case Constants.PAKISTANI_COMPANY.JAZZ.NAME:
                    pak_company_jazz.performClick();
                    break;
                case Constants.PAKISTANI_COMPANY.ZONG.NAME:
                    pak_company_zong.performClick();
                    break;
            }
        } else { // No company selected yet
            mPreview.setVisibility(View.INVISIBLE);
            bar.setVisibility(View.INVISIBLE);

            instruction_text.setText(getResources().getString(R.string.select_card_company));
        }
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to give required permission to the app in order to use it's feature");
                        }
                    }
                } else {
                    autoFocus = getActivity().getIntent().getBooleanExtra(AutoFocus, false);
                    useFlash = getActivity().getIntent().getBooleanExtra(UseFlash, false);
                    createCameraSource(autoFocus, useFlash);
                    torch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            useFlash = !useFlash;

                            if (useFlash) {
                                turnOnFlash();
                            } else {
                                turnOffFlash();
                            }
                        }
                    });
                }

                break;
        }
    }

    private void showMessageOKCancel(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermission();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getActivity().finish();
                    }
                })
                .create()
                .show();
    }

    private void previewCameraSetup() {
        if(SharedPrefUtil.getInstance().getTelecomeCompanySelected() == null) {
            mPreview.setVisibility(View.VISIBLE);
            bar.setVisibility(View.VISIBLE);

            instruction_text.setText(getResources().getString(R.string.scan_desription));

            setScanAnimation();
        }
    }

    private void setPakCompaniesOnClick() {
        pak_company_warid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewCameraSetup();

                SharedPrefUtil.getInstance().setTelecomeCompanySelected(Constants.PAKISTANI_COMPANY.WARID.NAME);
                SharedPrefUtil.getInstance().setDialerCodeSelected(Constants.PAKISTANI_COMPANY.WARID.DIALERCODE);
                SharedPrefUtil.getInstance().setPatternSelected(Constants.PAKISTANI_COMPANY.WARID.CARD_PATTERN);

                changeButtonSelectedState(v, pak_telecom_company_section);
            }
        });

        pak_company_ufone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewCameraSetup();

                SharedPrefUtil.getInstance().setTelecomeCompanySelected(Constants.PAKISTANI_COMPANY.UFONE.NAME);
                SharedPrefUtil.getInstance().setDialerCodeSelected(Constants.PAKISTANI_COMPANY.UFONE.DIALERCODE);
                SharedPrefUtil.getInstance().setPatternSelected(Constants.PAKISTANI_COMPANY.UFONE.CARD_PATTERN);

                changeButtonSelectedState(v, pak_telecom_company_section);
            }
        });

        pak_company_telenor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewCameraSetup();

                SharedPrefUtil.getInstance().setTelecomeCompanySelected(Constants.PAKISTANI_COMPANY.TELENOR.NAME);
                SharedPrefUtil.getInstance().setDialerCodeSelected(Constants.PAKISTANI_COMPANY.TELENOR.DIALERCODE);
                SharedPrefUtil.getInstance().setPatternSelected(Constants.PAKISTANI_COMPANY.TELENOR.CARD_PATTERN);

                changeButtonSelectedState(v, pak_telecom_company_section);
            }
        });

        pak_company_jazz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewCameraSetup();

                SharedPrefUtil.getInstance().setTelecomeCompanySelected(Constants.PAKISTANI_COMPANY.JAZZ.NAME);
                SharedPrefUtil.getInstance().setDialerCodeSelected(Constants.PAKISTANI_COMPANY.JAZZ.DIALERCODE);
                SharedPrefUtil.getInstance().setPatternSelected(Constants.PAKISTANI_COMPANY.JAZZ.CARD_PATTERN);

                changeButtonSelectedState(v, pak_telecom_company_section);
            }
        });

        pak_company_zong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewCameraSetup();

                SharedPrefUtil.getInstance().setTelecomeCompanySelected(Constants.PAKISTANI_COMPANY.ZONG.NAME);
                SharedPrefUtil.getInstance().setDialerCodeSelected(Constants.PAKISTANI_COMPANY.ZONG.DIALERCODE);
                SharedPrefUtil.getInstance().setPatternSelected(Constants.PAKISTANI_COMPANY.ZONG.CARD_PATTERN);

                changeButtonSelectedState(v, pak_telecom_company_section);
            }
        });
    }

    private void changeButtonSelectedState(View view, ViewGroup parentView) {
        for (int i = 0; i < parentView.getChildCount(); i++) {
            View v = parentView.getChildAt(i);
            if (v instanceof ImageView) {
                int value;
                int paddingValue = Math.round(MyLib.Dimension.convertDpToPixel(6));
                int marginValue = Math.round(MyLib.Dimension.convertDpToPixel(5));

                // It is selected element
                if (v.getId() == view.getId()) {
                    value = Math.round(MyLib.Dimension.convertDpToPixel(65));
                    v.setBackgroundResource(R.drawable.image_border_selected);

                } else {
                    value = Math.round(MyLib.Dimension.convertDpToPixel(50));
                    v.setBackgroundResource(R.drawable.image_border);
                }

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(value, value);
                params.setMarginStart(marginValue);
                params.setMarginEnd(marginValue);
                v.setPadding(paddingValue, paddingValue, paddingValue, paddingValue);
                v.setLayoutParams(params);
            }
        }

    }

    private void setScanAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.scan_animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bar.startAnimation(animation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        bar.startAnimation(animation);
    }

    /*
     * Turning On flash
     */
    private void turnOnFlash() {
        torch.setImageResource(R.drawable.icon_flash_light_onn);
        mCameraSource.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
    }

    /*
     * Turning Off flash
     */
    private void turnOffFlash() {
        torch.setImageResource(R.drawable.icon_flash_light_off);
        mCameraSource.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
//    private void requestCameraPermission() {
////        Log.w(TAG, "Camera permission is not granted. Requesting permission");
////
////        final String[] permissions = new String[]{Manifest.permission.CAMERA};
////
////        if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
////                Manifest.permission.CAMERA)) {
////            ActivityCompat.requestPermissions(getActivity(), permissions, RC_HANDLE_CAMERA_PERM);
////            return;
////        }
//        if (ContextCompat.checkSelfPermission(getContext(),
//                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//
//            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
//                    getContext(), Manifest.permission.CAMERA)) {
//
//
//            } else {
//                ActivityCompat.requestPermissions((Activity) getContext(),
//                        new String[]{Manifest.permission.CAMERA},
//                        MY_PERMISSIONS_REQUEST_CAMERA);
//            }
//
//        }
//        final Activity thisActivity = getActivity();
//
//        View.OnClickListener listener = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ActivityCompat.requestPermissions(thisActivity, permissions,
//                        RC_HANDLE_CAMERA_PERM);
//            }
//        };
//    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the ocr detector to detect small text samples
     * at long distances.
     * <p>
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */

    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getContext();

        // Create the TextRecognizer
        TextRecognizer textRecognizer = new TextRecognizer.Builder(context).build();

        // Set the TextRecognizer's Processor.
        textRecognizer.setProcessor(new OcrDetectorProcessor(mGraphicOverlay, mPreview, context, ID, SimOperatorName));

        // Check if the TextRecognizer is operational.
        if (!textRecognizer.isOperational()) {
            Log.w(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = getActivity().registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(getActivity(), R.string.low_storage_error, Toast.LENGTH_LONG).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        }

        //  Create the mCameraSource using the TextRecognizer.
        mCameraSource =
                new CameraSource.Builder(context, textRecognizer)
                        .setFacing(CameraSource.CAMERA_FACING_BACK)
                        .setRequestedPreviewSize(1280, 720)
                        .setRequestedFps(25.0f)
                        .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                        .setFocusMode(autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null)
                        .build();

        startCameraSource();

    }

    /**
     * Restarts the camera.
     */
    @Override
    public void onResume() {
        super.onResume();
        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    public void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        if (requestCode != RC_HANDLE_CAMERA_PERM) {
//            Log.d(TAG, "Got unexpected permission result: " + requestCode);
//            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//            return;
//        }
//
//        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Log.d(TAG, "Camera permission granted - initialize the camera source");
//            // We have permission, so create the camerasource
//            boolean autoFocus = getActivity().getIntent().getBooleanExtra(AutoFocus, false);
//            boolean useFlash = getActivity().getIntent().getBooleanExtra(UseFlash, false);
//            createCameraSource(autoFocus, useFlash);
//            return;
//        }
//
//        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
//                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));
//
//        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                getActivity().finish();
//            }
//        };
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setTitle("Multitracker sample")
//                .setMessage(R.string.no_camera_permission)
//                .setPositiveButton(R.string.ok, listener)
//                .show();
//    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getActivity());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    /**
     * onTap is called to speak the tapped TextBlock, if any, out loud.
     *
     * @param rawX - the raw position of the tap
     * @param rawY - the raw position of the tap.
     * @return true if the tap was on a TextBlock
     */
    private boolean onTap(float rawX, float rawY) {
        // TODO: Speak the text when the user taps on screen.
        return false;
    }

    private class CaptureGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return onTap(e.getRawX(), e.getRawY()) || super.onSingleTapConfirmed(e);
        }
    }

    private class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener {

        /**
         * Responds to scaling events for a gesture in progress.
         * Reported by pointer motion.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should consider this event
         * as handled. If an event was not handled, the detector
         * will continue to accumulate movement until an event is
         * handled. This can be useful if an application, for example,
         * only wants to update scaling factors if the change is
         * greater than 0.01.
         */
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return false;
        }

        /**
         * Responds to the beginning of a scaling gesture. Reported by
         * new pointers going down.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should continue recognizing
         * this gesture. For example, if a gesture is beginning
         * with a focal point outside of a region where it makes
         * sense, onScaleBegin() may return false to ignore the
         * rest of the gesture.
         */
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        /**
         * Responds to the end of a scale gesture. Reported by existing
         * pointers going up.
         * <p/>
         * Once a scale has ended, {@link ScaleGestureDetector#getFocusX()}
         * and {@link ScaleGestureDetector#getFocusY()} will return focal point
         * of the pointers remaining on the screen.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         */
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            if (mCameraSource != null) {
                mCameraSource.doZoom(detector.getScaleFactor());
            }
        }
    }
}
