package com.musahah.smart_iandroid.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.musahah.smart_iandroid.Application.BaseActivity;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.helper.CaptureActivityPortrait;
import com.musahah.smart_iandroid.helper.TinyDB;

import java.util.ArrayList;
import java.util.List;


public class QRBarcodeFragment extends Fragment {
    private TinyDB tinydb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_qr_code, container, false);

        // You can use key vol + and -. It is a solution i finded for torch .
        startReadingBarcode(view);

        return view;
    }

    private void startReadingBarcode(View view) {

    }

    private void setRetreivedBarcode(Barcode barcode) {
        tinydb = new TinyDB(getActivity());

        ArrayList<String> QRBarcodeHistory =  tinydb.getListString("Barcode_Scan_history");
        QRBarcodeHistory.add(barcode.displayValue);

        tinydb.putListString("Barcode_Scan_history", QRBarcodeHistory);

        // Success
        Toast.makeText(getContext(), "Scanned: " + barcode.displayValue, Toast.LENGTH_LONG).show();
    }

}
