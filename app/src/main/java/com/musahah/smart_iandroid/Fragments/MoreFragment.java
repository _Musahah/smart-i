package com.musahah.smart_iandroid.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bitvale.switcher.SwitcherX;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.Utils.AppConstants;
import com.musahah.smart_iandroid.Utils.SharedPrefUtil;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class MoreFragment extends Fragment {
    private ImageView emailNow;
    private SwitcherX switcher, copySwitcher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);

        emailNow = view.findViewById(R.id.email_now);
        emailNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{AppConstants.EMAIL_ADDRESS});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, AppConstants.EMAIL_SUBJECT);
                emailIntent.setType("message/rfc822");

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        switcher = view.findViewById(R.id.switcher);
        copySwitcher = view.findViewById(R.id.copy_switcher);

        setSwitch();

        return view;
    }

    private void setSwitch() {
        // Setting auto dial toggle button
        if(SharedPrefUtil.getInstance().getAutoDial())
            switcher.setChecked(true, true);
        else
            switcher.setChecked(false, true);

        // Setting copy toggle button
        if(SharedPrefUtil.getInstance().getCopyScanned())
            copySwitcher.setChecked(true, true);
        else
            copySwitcher.setChecked(false, true);

        switcher.setOnCheckedChangeListener(new Function1<Boolean, Unit>() {
            @Override
            public Unit invoke(Boolean aBoolean) {
                if(aBoolean)
                    SharedPrefUtil.getInstance().setAutoDial(true);
                else
                    SharedPrefUtil.getInstance().setAutoDial(false);
                return null;
            }
        });

        copySwitcher.setOnCheckedChangeListener(new Function1<Boolean, Unit>() {
            @Override
            public Unit invoke(Boolean aBoolean) {
                if(aBoolean)
                    SharedPrefUtil.getInstance().setCopyScanned(true);
                else
                    SharedPrefUtil.getInstance().setCopyScanned(false);
                return null;
            }
        });
    }
}