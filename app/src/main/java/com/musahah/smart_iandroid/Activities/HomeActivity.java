package com.musahah.smart_iandroid.Activities;

import android.os.Bundle;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.gauravk.bubblenavigation.BubbleNavigationConstraintView;
import com.gauravk.bubblenavigation.listener.BubbleNavigationChangeListener;
import com.musahah.smart_iandroid.Application.BaseActivity;
import com.musahah.smart_iandroid.Fragments.OcrCaptureFragment;
import com.musahah.smart_iandroid.MyLib.MyLib;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.ViewPager.HomePagerAdapter;

public class HomeActivity extends BaseActivity {
    private ViewPager viewPager;
    private BubbleNavigationConstraintView top_navigation_constraint;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_home);

        initComponent();

        HomePagerAdapter myPagerAdapter = new HomePagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(myPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                top_navigation_constraint.setCurrentActiveItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        top_navigation_constraint.setNavigationChangeListener(new BubbleNavigationChangeListener() {
            @Override
            public void onNavigationChanged(View view, int position) {
                viewPager.setCurrentItem(position, true);
            }
        });
    }

    private void initComponent() {
        top_navigation_constraint = findViewById(R.id.top_navigation_constraint);
        viewPager = findViewById(R.id.view_pager);
    }
}