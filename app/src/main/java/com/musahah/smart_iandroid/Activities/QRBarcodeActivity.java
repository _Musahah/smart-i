package com.musahah.smart_iandroid.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.musahah.smart_iandroid.Application.BaseActivity;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.helper.CaptureActivityPortrait;
import com.musahah.smart_iandroid.helper.TinyDB;

import java.util.ArrayList;

public class QRBarcodeActivity extends BaseActivity {
    TinyDB tinydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // You can use key vol + and -. It is a solution i finded for torch .
        startReadingBarcode();
    }

    private void startReadingBarcode() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if(result.getContents() == null){
                // Cancelled
            } else {
                tinydb = new TinyDB(this);

                ArrayList<String> QRBarcodeHistory =  tinydb.getListString("Barcode_Scan_history");
                QRBarcodeHistory.add(result.getContents());

                tinydb.putListString("Barcode_Scan_history", QRBarcodeHistory);

                // Success
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }
}
