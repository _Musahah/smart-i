package com.musahah.smart_iandroid.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.emredavarci.noty.Noty;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.musahah.smart_iandroid.Application.BaseActivity;
import com.musahah.smart_iandroid.MyLib.MyLib;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.adapters.CountryAdapter;
import com.musahah.smart_iandroid.localDatabase.models.Country;
import com.musahah.smart_iandroid.localDatabase.repositories.CountryRepository;
import com.musahah.smart_iandroid.network.BaseCallBack;
import com.musahah.smart_iandroid.network.RestManager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class CountryActivity extends BaseActivity implements CountryAdapter.OnShareClickedListener{

    private ShimmerFrameLayout mShimmerViewContainer;
    private RecyclerView recyclerView;
    private LinearLayout retry_network_section, no_result_section;
    private ImageView retry;
    private BroadcastReceiver br;
    private CountryAdapter nListAdapter;
    private ArrayList hideElementArray, showElementArray;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        retry_network_section = findViewById(R.id.retry_network_section);
        no_result_section = findViewById(R.id.no_result_section);
        retry = findViewById(R.id.retry_network);
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);

        recyclerView = findViewById(R.id.countrylist_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(CountryActivity.this));

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mSwipeRefreshLayout.setRefreshing(false);
            apiCallGetCountries();
        });

        apiCallGetCountries();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();

        // Unregister receiver
        if (br != null){
            unregisterReceiver(br);
            br = null;
        }

        super.onPause();
    }

    private void apiCallGetCountries() {
        setAdapterSharedClickCallback(new ArrayList<>());

        // Stopping Shimmer Effect's animation after data is loaded to ListView
        mShimmerViewContainer.startShimmerAnimation();

        // Hide elements
        hideElementArray = new ArrayList();
        hideElementArray.add(retry_network_section);
        hideElementArray.add(mSwipeRefreshLayout);
        hideElementArray.add(no_result_section);
        MyLib.System.hideElements(hideElementArray);

        // Show elements
        showElementArray = new ArrayList();
        showElementArray.add(mShimmerViewContainer);
        MyLib.System.showElements(showElementArray);

        if(MyLib.System.isNetworkAvailable(CountryActivity.this)){
            RestManager.getRestManagerInstance(false).getServices().getCountries()
                    .enqueue(new BaseCallBack<>((t, response) -> {
                        if(response != null) {
                                if (response.code() == 200) {
                                    if(response.isSuccessful()) {
                                        assert response.body() != null;
                                        if (response.body().size() == 0) {
                                            // Stopping Shimmer Effect's animation after data is loaded to ListView
                                            mShimmerViewContainer.stopShimmerAnimation();

                                            // Hide elements
                                            hideElementArray = new ArrayList();
                                            hideElementArray.add(mShimmerViewContainer);
                                            hideElementArray.add(retry_network_section);
                                            MyLib.System.hideElements(hideElementArray);

                                            // Show elements
                                            showElementArray = new ArrayList();
                                            showElementArray.add(mSwipeRefreshLayout);
                                            showElementArray.add(no_result_section);
                                            MyLib.System.showElements(showElementArray);
                                        } else {
                                            // Delete previous countries and insert the updated record again
                                            deleteCountries();

                                            // Set response countries on local database for offline mode
                                            setCountriesToLocalDatabase(response);

                                            setAdapterSharedClickCallback(response.body());

                                            // Stopping Shimmer Effect's animation after data is loaded to ListView
                                            mShimmerViewContainer.stopShimmerAnimation();

                                            // Hide elements
                                            hideElementArray = new ArrayList();
                                            hideElementArray.add(mShimmerViewContainer);
                                            hideElementArray.add(no_result_section);
                                            hideElementArray.add(retry_network_section);

                                            MyLib.System.hideElements(hideElementArray);

                                            // Show elements
                                            showElementArray = new ArrayList();
                                            showElementArray.add(mSwipeRefreshLayout);
                                            MyLib.System.showElements(showElementArray);
                                        }
                                    } else {
                                        // Stopping Shimmer Effect's animation after data is loaded to ListView
                                        mShimmerViewContainer.stopShimmerAnimation();

                                        // Hide elements
                                        hideElementArray = new ArrayList();
                                        hideElementArray.add(mShimmerViewContainer);
                                        hideElementArray.add(no_result_section);
                                        MyLib.System.hideElements(hideElementArray);

                                        // Show elements
                                        showElementArray = new ArrayList();
                                        showElementArray.add(retry_network_section);
                                        showElementArray.add(mSwipeRefreshLayout);
                                        MyLib.System.showElements(showElementArray);

                                        assert response.errorBody() != null;
                                        MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                                                MyLib.Retrofit.showErrorResponseMessageByValue(response.errorBody()),
                                                2500);
                                    }
                                } else if (response.code() == 500) {
                                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                                    mShimmerViewContainer.stopShimmerAnimation();

                                    // Hide elements
                                    hideElementArray = new ArrayList();
                                    hideElementArray.add(mShimmerViewContainer);
                                    hideElementArray.add(no_result_section);
                                    MyLib.System.hideElements(hideElementArray);

                                    // Show elements
                                    showElementArray = new ArrayList();
                                    showElementArray.add(retry_network_section);
                                    showElementArray.add(mSwipeRefreshLayout);
                                    MyLib.System.showElements(showElementArray);

                                    MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                                            getResources().getString(R.string.internal_server_error),
                                            2500);

                                } else if (response.code() == 504) {
                                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                                    mShimmerViewContainer.stopShimmerAnimation();

                                    // Hide elements
                                    hideElementArray = new ArrayList();
                                    hideElementArray.add(mShimmerViewContainer);
                                    hideElementArray.add(no_result_section);
                                    MyLib.System.hideElements(hideElementArray);

                                    // Show elements
                                    showElementArray = new ArrayList();
                                    showElementArray.add(retry_network_section);
                                    showElementArray.add(mSwipeRefreshLayout);
                                    MyLib.System.showElements(showElementArray);

                                    MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                                            getResources().getString(R.string.target_not_responded),
                                            3500);

                                } else if (response.code() == 401 || response.code() == 403) {
                                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                                    mShimmerViewContainer.stopShimmerAnimation();

                                    // Hide elements
                                    hideElementArray = new ArrayList();
                                    hideElementArray.add(mShimmerViewContainer);
                                    hideElementArray.add(no_result_section);
                                    MyLib.System.hideElements(hideElementArray);

                                    // Show elements
                                    showElementArray = new ArrayList();
                                    showElementArray.add(retry_network_section);
                                    showElementArray.add(mSwipeRefreshLayout);
                                    MyLib.System.showElements(showElementArray);

                                    MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                                            getResources().getString(R.string.access_denied),
                                            3500);

                                } else if (response.code() == 404 ) {
                                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                                    mShimmerViewContainer.stopShimmerAnimation();

                                    // Hide elements
                                    hideElementArray = new ArrayList();
                                    hideElementArray.add(mShimmerViewContainer);
                                    hideElementArray.add(no_result_section);
                                    MyLib.System.hideElements(hideElementArray);

                                    // Show elements
                                    showElementArray = new ArrayList();
                                    showElementArray.add(retry_network_section);
                                    showElementArray.add(mSwipeRefreshLayout);
                                    MyLib.System.showElements(showElementArray);

                                    MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                                            getResources().getString(R.string.not_found),
                                            3000);
                                }
                        }
                        else { // On Failure
                            // Stopping Shimmer Effect's animation after data is loaded to ListView
                            mShimmerViewContainer.stopShimmerAnimation();

                            // Hide elements
                            hideElementArray = new ArrayList();
                            hideElementArray.add(mShimmerViewContainer);
                            hideElementArray.add(no_result_section);
                            MyLib.System.hideElements(hideElementArray);

                            // Show elements
                            showElementArray = new ArrayList();
                            showElementArray.add(retry_network_section);
                            showElementArray.add(mSwipeRefreshLayout);
                            MyLib.System.showElements(showElementArray);

                            MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                                     t.getMessage(),
                                    6000);
                        }
                    }, false, CountryActivity.this));
        } else {
            CountryRepository countryRepository = new CountryRepository(getApplicationContext());

            countryRepository.count().observe(CountryActivity.this, count -> {
                assert count != null;
                if(count != 0) {
                    countryRepository.getCountries().observe(CountryActivity.this, this::setAdapterSharedClickCallback);

                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                    mShimmerViewContainer.stopShimmerAnimation();

                    // Hide elements
                    hideElementArray = new ArrayList();
                    hideElementArray.add(no_result_section);
                    hideElementArray.add(retry_network_section);
                    hideElementArray.add(mShimmerViewContainer);
                    MyLib.System.hideElements(hideElementArray);

                    // Show elements
                    showElementArray = new ArrayList();
                    showElementArray.add(mSwipeRefreshLayout);
                    MyLib.System.showElements(showElementArray);

                    MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                            getResources().getString(R.string.offline_mode),
                            3500);

                    checkInternetInBackground();
                } else {
                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                    mShimmerViewContainer.stopShimmerAnimation();

                    // Hide elements
                    hideElementArray = new ArrayList();
                    hideElementArray.add(mShimmerViewContainer);
                    hideElementArray.add(no_result_section);
                    MyLib.System.hideElements(hideElementArray);

                    // Show elements
                    showElementArray = new ArrayList();
                    showElementArray.add(retry_network_section);
                    showElementArray.add(mSwipeRefreshLayout);
                    MyLib.System.showElements(showElementArray);

                    MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                            getResources().getString(R.string.no_internet),
                            2500);

                    checkInternetInBackground();
                }
            });
        }
    }

    private void deleteCountries() {
        CountryRepository countryRepository = new CountryRepository(getApplicationContext());
        countryRepository.deleteCountries();
    }

    private void checkInternetInBackground(){
        // Check for internet connection in background
        if (br == null) {
            br = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    Bundle extras = intent.getExtras();

                    NetworkInfo info = extras
                            .getParcelable("networkInfo");

                    NetworkInfo.State state = info.getState();
                    Log.d("TEST Internet", info.toString() + " "
                            + state.toString());

                    if (state == NetworkInfo.State.CONNECTED) {
                        MyLib.Feedback.showSnackForCertainTime(CountryActivity.this,
                                getResources().getString(R.string.back_online),
                                2000);

                        apiCallGetCountries();
                    }
                }
            };

            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(br, intentFilter);
        }
    }

    private void setAdapterSharedClickCallback(List<Country> countryList) {
        nListAdapter = new CountryAdapter(CountryActivity.this, countryList);
        nListAdapter.setOnShareClickedListener(this);
        recyclerView.setAdapter(nListAdapter);
    }

    private void setCountriesToLocalDatabase(Response<List<Country>> response) {
        CountryRepository countryRepository = new CountryRepository(getApplicationContext());

        assert response.body() != null;
        countryRepository.insertCountries(response.body());
    }

    @Override
    public void ShareClicked(String countryNid) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            // Do something for lollipop and above versions
            MyLib.ANIMATION.singleSharedElementTransitionPostLollipop(CountryActivity.this, TelecomCompanyActivity.class, findViewById(R.id.logo));
        } else{
            // do something for phones running an SDK before lollipop
            Intent i = new Intent(CountryActivity.this,
                    TelecomCompanyActivity.class);
            //Intent is used to switch from one activity to another.

            startActivity(i);
            //invoke the SecondActivity
        }

//        Intent myIntent = new Intent(CountryActivity.this, TelecomCompanyActivity.class);
//        myIntent.putExtra("countryNid", countryNid);
//        startActivity(myIntent);
    }
}
