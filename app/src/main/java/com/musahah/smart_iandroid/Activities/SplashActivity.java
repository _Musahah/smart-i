package com.musahah.smart_iandroid.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;

import com.musahah.smart_iandroid.Application.BaseActivity;
import com.musahah.smart_iandroid.MyLib.MyLib;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.localDatabase.models.Country;
import com.musahah.smart_iandroid.localDatabase.repositories.CountryRepository;
import com.musahah.smart_iandroid.network.BaseCallBack;
import com.musahah.smart_iandroid.network.RestManager;

import java.util.List;

import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    public static int SPLASH_SCREEN_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    // Do something for lollipop and above versions
                    MyLib.ANIMATION.singleSharedElementTransitionPostLollipop(SplashActivity.this, CountryActivity.class, findViewById(R.id.logo));
                } else{
                    // do something for phones running an SDK before lollipop
                    Intent i = new Intent(SplashActivity.this,
                            CountryActivity.class);
                    //Intent is used to switch from one activity to another.

                    startActivity(i);
                    //invoke the SecondActivity
                }
            }
        }, SPLASH_SCREEN_TIME_OUT);
    }
}
