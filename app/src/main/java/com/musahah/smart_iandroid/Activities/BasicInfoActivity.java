package com.musahah.smart_iandroid.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.musahah.smart_iandroid.Application.BaseActivity;
import com.musahah.smart_iandroid.R;

public class BasicInfoActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_info);
    }
}
