package com.musahah.smart_iandroid.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.musahah.smart_iandroid.Application.BaseActivity;
import com.musahah.smart_iandroid.MyLib.MyLib;
import com.musahah.smart_iandroid.R;
import com.musahah.smart_iandroid.adapters.CountryAdapter;
import com.musahah.smart_iandroid.adapters.TelecomCompanyAdapter;
import com.musahah.smart_iandroid.localDatabase.models.Country;
import com.musahah.smart_iandroid.localDatabase.models.TelecomCompany;
import com.musahah.smart_iandroid.localDatabase.repositories.CountryRepository;
import com.musahah.smart_iandroid.localDatabase.repositories.TelecomCompanyRepository;
import com.musahah.smart_iandroid.network.BaseCallBack;
import com.musahah.smart_iandroid.network.RestManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Response;

public class TelecomCompanyActivity extends BaseActivity implements TelecomCompanyAdapter.OnShareClickedListener {
    private String countryNid = "1";

    private ShimmerFrameLayout mShimmerViewContainer;
    private RecyclerView recyclerView;
    private LinearLayout retry_network_section, no_result_section;
    private ImageView retry;
    private BroadcastReceiver br;
    private TelecomCompanyAdapter nListAdapter;
    private ArrayList hideElementArray, showElementArray;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telecom_company);

//        Intent mIntent = getIntent();
//        countryNid = Objects.requireNonNull(mIntent.getExtras()).getString("countryNid", "1");
//
//        MyLib.Feedback.showSnackForInfiniteTime(TelecomCompanyActivity.this, "CountryId: " + countryNid);


        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        retry_network_section = findViewById(R.id.retry_network_section);
        no_result_section = findViewById(R.id.no_result_section);
        retry = findViewById(R.id.retry_network);
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);

        recyclerView = findViewById(R.id.telecomlist_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(TelecomCompanyActivity.this));

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mSwipeRefreshLayout.setRefreshing(false);
            apiCallGetTelecomCompanies();
        });

        apiCallGetTelecomCompanies();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();

        // Unregister receiver
        if (br != null){
            unregisterReceiver(br);
            br = null;
        }

        super.onPause();
    }

    private void apiCallGetTelecomCompanies() {
        setAdapterSharedClickCallback(new ArrayList<>());

        // Stopping Shimmer Effect's animation after data is loaded to ListView
        mShimmerViewContainer.startShimmerAnimation();

        // Hide elements
        hideElementArray = new ArrayList();
        hideElementArray.add(retry_network_section);
        hideElementArray.add(mSwipeRefreshLayout);
        hideElementArray.add(no_result_section);
        MyLib.System.hideElements(hideElementArray);

        // Show elements
        showElementArray = new ArrayList();
        showElementArray.add(mShimmerViewContainer);
        MyLib.System.showElements(showElementArray);

        if(MyLib.System.isNetworkAvailable(TelecomCompanyActivity.this)){
            RestManager.getRestManagerInstance(false).getServices().getTelecomCompanies(countryNid)
                    .enqueue(new BaseCallBack<>((t, response) -> {
                        if(response != null) {
                            if (response.code() == 200) {
                                if(response.isSuccessful()) {
                                    assert response.body() != null;
                                    if (response.body().size() == 0) {
                                        // Stopping Shimmer Effect's animation after data is loaded to ListView
                                        mShimmerViewContainer.stopShimmerAnimation();

                                        // Hide elements
                                        hideElementArray = new ArrayList();
                                        hideElementArray.add(mShimmerViewContainer);
                                        hideElementArray.add(retry_network_section);
                                        MyLib.System.hideElements(hideElementArray);

                                        // Show elements
                                        showElementArray = new ArrayList();
                                        showElementArray.add(mSwipeRefreshLayout);
                                        showElementArray.add(no_result_section);
                                        MyLib.System.showElements(showElementArray);
                                    } else {
                                        // Delete previous telecom companies and insert the updated record again
                                        deleteTelecomCompanies();

                                        // Set response telecom companies on local database for offline mode
                                        setTelecomCompaniesToLocalDatabase(response);

                                        setAdapterSharedClickCallback(response.body());

                                        // Stopping Shimmer Effect's animation after data is loaded to ListView
                                        mShimmerViewContainer.stopShimmerAnimation();

                                        // Hide elements
                                        hideElementArray = new ArrayList();
                                        hideElementArray.add(mShimmerViewContainer);
                                        hideElementArray.add(no_result_section);
                                        hideElementArray.add(retry_network_section);

                                        MyLib.System.hideElements(hideElementArray);

                                        // Show elements
                                        showElementArray = new ArrayList();
                                        showElementArray.add(mSwipeRefreshLayout);
                                        MyLib.System.showElements(showElementArray);
                                    }
                                } else {
                                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                                    mShimmerViewContainer.stopShimmerAnimation();

                                    // Hide elements
                                    hideElementArray = new ArrayList();
                                    hideElementArray.add(mShimmerViewContainer);
                                    hideElementArray.add(no_result_section);
                                    MyLib.System.hideElements(hideElementArray);

                                    // Show elements
                                    showElementArray = new ArrayList();
                                    showElementArray.add(retry_network_section);
                                    showElementArray.add(mSwipeRefreshLayout);
                                    MyLib.System.showElements(showElementArray);

                                    assert response.errorBody() != null;
                                    MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                                            MyLib.Retrofit.showErrorResponseMessageByValue(response.errorBody()),
                                            2500);
                                }
                            } else if (response.code() == 500) {
                                // Stopping Shimmer Effect's animation after data is loaded to ListView
                                mShimmerViewContainer.stopShimmerAnimation();

                                // Hide elements
                                hideElementArray = new ArrayList();
                                hideElementArray.add(mShimmerViewContainer);
                                hideElementArray.add(no_result_section);
                                MyLib.System.hideElements(hideElementArray);

                                // Show elements
                                showElementArray = new ArrayList();
                                showElementArray.add(retry_network_section);
                                showElementArray.add(mSwipeRefreshLayout);
                                MyLib.System.showElements(showElementArray);

                                MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                                        getResources().getString(R.string.internal_server_error),
                                        2500);

                            } else if (response.code() == 504) {
                                // Stopping Shimmer Effect's animation after data is loaded to ListView
                                mShimmerViewContainer.stopShimmerAnimation();

                                // Hide elements
                                hideElementArray = new ArrayList();
                                hideElementArray.add(mShimmerViewContainer);
                                hideElementArray.add(no_result_section);
                                MyLib.System.hideElements(hideElementArray);

                                // Show elements
                                showElementArray = new ArrayList();
                                showElementArray.add(retry_network_section);
                                showElementArray.add(mSwipeRefreshLayout);
                                MyLib.System.showElements(showElementArray);

                                MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                                        getResources().getString(R.string.target_not_responded),
                                        3500);

                            } else if (response.code() == 401 || response.code() == 403) {
                                // Stopping Shimmer Effect's animation after data is loaded to ListView
                                mShimmerViewContainer.stopShimmerAnimation();

                                // Hide elements
                                hideElementArray = new ArrayList();
                                hideElementArray.add(mShimmerViewContainer);
                                hideElementArray.add(no_result_section);
                                MyLib.System.hideElements(hideElementArray);

                                // Show elements
                                showElementArray = new ArrayList();
                                showElementArray.add(retry_network_section);
                                showElementArray.add(mSwipeRefreshLayout);
                                MyLib.System.showElements(showElementArray);

                                MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                                        getResources().getString(R.string.access_denied),
                                        3500);

                            } else if (response.code() == 404 ) {
                                // Stopping Shimmer Effect's animation after data is loaded to ListView
                                mShimmerViewContainer.stopShimmerAnimation();

                                // Hide elements
                                hideElementArray = new ArrayList();
                                hideElementArray.add(mShimmerViewContainer);
                                hideElementArray.add(no_result_section);
                                MyLib.System.hideElements(hideElementArray);

                                // Show elements
                                showElementArray = new ArrayList();
                                showElementArray.add(retry_network_section);
                                showElementArray.add(mSwipeRefreshLayout);
                                MyLib.System.showElements(showElementArray);

                                MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                                        getResources().getString(R.string.not_found),
                                        3000);
                            }
                        }
                        else { // On Failure
                            // Stopping Shimmer Effect's animation after data is loaded to ListView
                            mShimmerViewContainer.stopShimmerAnimation();

                            // Hide elements
                            hideElementArray = new ArrayList();
                            hideElementArray.add(mShimmerViewContainer);
                            hideElementArray.add(no_result_section);
                            MyLib.System.hideElements(hideElementArray);

                            // Show elements
                            showElementArray = new ArrayList();
                            showElementArray.add(retry_network_section);
                            showElementArray.add(mSwipeRefreshLayout);
                            MyLib.System.showElements(showElementArray);

                            MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                                    t.getMessage(),
                                    6000);
                        }
                    }, false, TelecomCompanyActivity.this));
        } else {
            TelecomCompanyRepository telecomCompanyRepository = new TelecomCompanyRepository(getApplicationContext());

            telecomCompanyRepository.count().observe(TelecomCompanyActivity.this, count -> {
                assert count != null;
                if(count != 0) {
                    telecomCompanyRepository.getTelecomCompanies().observe(TelecomCompanyActivity.this, this::setAdapterSharedClickCallback);

                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                    mShimmerViewContainer.stopShimmerAnimation();

                    // Hide elements
                    hideElementArray = new ArrayList();
                    hideElementArray.add(no_result_section);
                    hideElementArray.add(retry_network_section);
                    hideElementArray.add(mShimmerViewContainer);
                    MyLib.System.hideElements(hideElementArray);

                    // Show elements
                    showElementArray = new ArrayList();
                    showElementArray.add(mSwipeRefreshLayout);
                    MyLib.System.showElements(showElementArray);

                    MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                            getResources().getString(R.string.offline_mode),
                            3500);

                    checkInternetInBackground();
                } else {
                    // Stopping Shimmer Effect's animation after data is loaded to ListView
                    mShimmerViewContainer.stopShimmerAnimation();

                    // Hide elements
                    hideElementArray = new ArrayList();
                    hideElementArray.add(mShimmerViewContainer);
                    hideElementArray.add(no_result_section);
                    MyLib.System.hideElements(hideElementArray);

                    // Show elements
                    showElementArray = new ArrayList();
                    showElementArray.add(retry_network_section);
                    showElementArray.add(mSwipeRefreshLayout);
                    MyLib.System.showElements(showElementArray);

                    MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                            getResources().getString(R.string.no_internet),
                            2500);

                    checkInternetInBackground();
                }
            });
        }
    }

    private void deleteTelecomCompanies() {
        TelecomCompanyRepository telecomCompanyRepository = new TelecomCompanyRepository(getApplicationContext());
        telecomCompanyRepository.deleteTelecomCompanies();
    }

    private void checkInternetInBackground(){
        // Check for internet connection in background
        if (br == null) {
            br = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    Bundle extras = intent.getExtras();

                    NetworkInfo info = extras
                            .getParcelable("networkInfo");

                    NetworkInfo.State state = info.getState();
                    Log.d("TEST Internet", info.toString() + " "
                            + state.toString());

                    if (state == NetworkInfo.State.CONNECTED) {
                        MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                                getResources().getString(R.string.back_online),
                                2000);

                        apiCallGetTelecomCompanies();
                    }
                }
            };

            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(br, intentFilter);
        }
    }

    private void setAdapterSharedClickCallback(List<TelecomCompany> telecomCompanyList) {
        nListAdapter = new TelecomCompanyAdapter(TelecomCompanyActivity.this, telecomCompanyList);
        nListAdapter.setOnShareClickedListener(this);
        recyclerView.setAdapter(nListAdapter);
    }

    private void setTelecomCompaniesToLocalDatabase(Response<List<TelecomCompany>> response) {
        TelecomCompanyRepository telecomCompanyRepository = new TelecomCompanyRepository(getApplicationContext());

        assert response.body() != null;
        telecomCompanyRepository.insertTelecomCompanies(response.body());
    }

    @Override
    public void ShareClicked(String countryNid) {
//        Intent myIntent = new Intent(TelecomCompanyActivity.this, TelecomCompanyActivity.class);
//        myIntent.putExtra("countryNid", countryNid);
//        startActivity(myIntent);

        MyLib.Feedback.showSnackForCertainTime(TelecomCompanyActivity.this,
                countryNid,
                2000);
    }
}
