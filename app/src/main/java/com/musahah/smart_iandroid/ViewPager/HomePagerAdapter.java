package com.musahah.smart_iandroid.ViewPager;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.musahah.smart_iandroid.Fragments.MoreFragment;
import com.musahah.smart_iandroid.Fragments.OcrCaptureFragment;
import com.musahah.smart_iandroid.Fragments.TextFromImageFragment;
import com.musahah.smart_iandroid.R;


public class HomePagerAdapter extends FragmentStatePagerAdapter {

    private Context mContext;

    public HomePagerAdapter(@NonNull FragmentManager fm, Context context) {
        super(fm);

        mContext = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new OcrCaptureFragment();
            case 1:
                return new MoreFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.scan_card);
            case 1:
                return mContext.getString(R.string.more);
            default:
                return null;
        }
    }
}
