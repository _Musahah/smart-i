package com.musahah.smart_iandroid.Utils;

public interface AppConstants {
    int SPLASH_TIME_OUT = 1800; // 2 seconds

    // Database name
    String DB_NAME = "App_Db";

    // Shared pref file name
    String PREF_NAME = "App_Prefs";

    // API call base url
    String BASE_URL = "https://dev-smarti-scan.pantheonsite.io";

    // Typeface path
    String TYPEFACE_PATH = "fonts/CYBERTOOTH.otf";

    // Email
    String EMAIL_ADDRESS = "hi.musahah@gmail.com";

    // Email Subject
    String EMAIL_SUBJECT = "Smart-i Feedback";
}
