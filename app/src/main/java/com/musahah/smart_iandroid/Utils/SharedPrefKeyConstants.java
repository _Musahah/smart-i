package com.musahah.smart_iandroid.Utils;

import java.util.HashMap;
import java.util.Map;

public interface SharedPrefKeyConstants {
    /* START Shared Prefs AppConstants */
    // Check is user loggedIn or not
    String KEY_IS_LOGIN = "IsLoggedIn";

    // Selected country
    String KEY_COUNTRY_CODE = "KEY_COUNTRY_CODE";

    // Selected telecom company
    String KEY_TELECOM_COMPANY = "KEY_TELECOM_COMPANY";

    // Selected telecom company card pattern
    String KEY_PATTERN = "KEY_PATTERN";

    // Selected telecom company'S dialer code
    String KEY_DAILER_CODE = "KEY_DAILER_CODE";

    // Auto dial
    String KEY_AUTO_DIAL = "KEY_AUTO_DIAL";

    // Copy scanned
    String KEY_COPY_SCANNED = "KEY_COPY_SCANNED";

    // Language header for Api
    String KEY_LANG_CODE = "langCode";

    // Run only once
    String KEY_FIRST_RUN = "firstRun";

    // Languages1 api call only once
    String KEY_API_FIRST_RUN = "apiFirstRun";

    // Layout direction
    String KEY_LAYOUT_DIRECTION = "layoutDirection";

    // Selected Language Id
    String KEY_LANG_ID = "langId";

    // Selected Invoice Language Id
    String KEY_INVOICE_LANG_ID = "invoiceLangId";

    // Select invoice Language code
    String KEY_INVOICE_LANG_CODE = "invoiceLangCode";

    // Branch code
    String KEY_BRANCH_CODE = "branchCode";

    // Token for Api call
    String KEY_API_TOKEN = "token";

    // Token expiration in second for Api call
    String KEY_API_TOKEN_EXPIRE_IN_SECONDS = "tokenExpireInSeconds";

    // is synced
    String KEY_SYNCED = "synced";

    // Token for Api call
    String KEY_USER_ID = "userId";

    // Token for Api call
    String KEY_NATIONAL_ID = "nationalId";

    // LoggedIn User name
    String KEY_USER_NAME = "userName";

    // Should user change password
    String KEY_SHOULD_CHANGE_PASSWORD = "shouldChangePassword";

    // Device Id
    String KEY_DEVICE_ID = "deviceId";

    // Base url for api calls
    String KEY_BASE_API_URL = "baseApiUrl";

    // Show Intro (make variable public to access from outside)
    String KEY_FLAG_SHOW_INTRO = "showIntro";
    /*END Shared Prefs AppConstants */
}
