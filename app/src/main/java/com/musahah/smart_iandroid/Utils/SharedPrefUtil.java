package com.musahah.smart_iandroid.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.musahah.smart_iandroid.Application.App;

public class SharedPrefUtil {
    private SharedPreferences mSharedPreferences;
    private static SharedPrefUtil mInstance;

    private SharedPrefUtil() {
        if (mSharedPreferences == null) {
            mSharedPreferences = App.getApplicaionInstance().getSharedPreferences(
                    AppConstants.PREF_NAME, Context.MODE_PRIVATE);
        }
    }

    // synchronized is used if multiple methods calling the same instance at the same time
    public static synchronized SharedPrefUtil getInstance() {
        if (mInstance == null) {
            mInstance = new SharedPrefUtil();
        }
        return mInstance;
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }

    public void clearSharedPreferences(){
        mSharedPreferences.edit().clear().apply();
    }

    public void setFirstRun(boolean firstRun) {
        mSharedPreferences.edit().putBoolean(SharedPrefKeyConstants.KEY_FIRST_RUN, firstRun).apply();
    }
    public boolean getFirstRun() {
        return mSharedPreferences.getBoolean(SharedPrefKeyConstants.KEY_FIRST_RUN, true);
    }


    public void setDeviceId(String deviceId){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_DEVICE_ID, deviceId).apply();
    }
    public String getDeviceId() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_DEVICE_ID, null);
    }


    public void setBaseApiUrl(String baseApiUrl){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_BASE_API_URL, baseApiUrl).apply();
    }
    public String getBaseApiUrl() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_BASE_API_URL, null);
    }


    public void setUserName(String userName){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_USER_NAME, userName).apply();
    }
    public String getUserName() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_USER_NAME, null);
    }


    public void setToken(String token){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_API_TOKEN, token).apply();
    }
    public String getToken() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_API_TOKEN, null);
    }


    public int getUserId() {
        return mSharedPreferences.getInt(SharedPrefKeyConstants.KEY_USER_ID, 0);
    }
    public void setUserId(int userId){
        mSharedPreferences.edit().putInt(SharedPrefKeyConstants.KEY_USER_ID, userId).apply();
    }

    public String getNationalId() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_NATIONAL_ID, "");
    }
    public void setNationalId(String nationalID){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_NATIONAL_ID, nationalID).apply();
    }


    public void setShouldChangePassword(boolean shouldChangePassword) {
        mSharedPreferences.edit().putBoolean(SharedPrefKeyConstants.KEY_SHOULD_CHANGE_PASSWORD, shouldChangePassword).apply();
    }
    public boolean getShouldChangePassword() {
        return mSharedPreferences.getBoolean(SharedPrefKeyConstants.KEY_SHOULD_CHANGE_PASSWORD, false);
    }


    public long getTokenExpireInSeconds() {
        return mSharedPreferences.getLong(SharedPrefKeyConstants.KEY_API_TOKEN_EXPIRE_IN_SECONDS, 0);
    }
    public void setTokenExpireInSeconds(long tokenExpireInSeconds){
        mSharedPreferences.edit().putLong(SharedPrefKeyConstants.KEY_API_TOKEN_EXPIRE_IN_SECONDS, tokenExpireInSeconds).apply();
    }


    public void setLogin(boolean login) {
        mSharedPreferences.edit().putBoolean(SharedPrefKeyConstants.KEY_IS_LOGIN, login).apply();
    }
    public boolean getLogin() {
        return mSharedPreferences.getBoolean(SharedPrefKeyConstants.KEY_IS_LOGIN, false);
    }


    public void setAutoDial(boolean autoDial) {
        mSharedPreferences.edit().putBoolean(SharedPrefKeyConstants.KEY_AUTO_DIAL, autoDial).apply();
    }
    public boolean getAutoDial() {
        return mSharedPreferences.getBoolean(SharedPrefKeyConstants.KEY_AUTO_DIAL, false);
    }

    public void setCopyScanned(boolean copyScanned) {
        mSharedPreferences.edit().putBoolean(SharedPrefKeyConstants.KEY_COPY_SCANNED, copyScanned).apply();
    }
    public boolean getCopyScanned() {
        return mSharedPreferences.getBoolean(SharedPrefKeyConstants.KEY_COPY_SCANNED, false);
    }

    public void setCountryCode(String countryCode){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_COUNTRY_CODE, countryCode).apply();
    }
    public String getCountryCode() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_COUNTRY_CODE, null);
    }

    public void setLanguageCode(String languageCode){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_LANG_CODE, languageCode).apply();
    }
    public String getLanguageCode() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_LANG_CODE, null);
    }

    public void setTelecomeCompanySelected(String telecomeCompanySelected){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_TELECOM_COMPANY, telecomeCompanySelected).apply();
    }
    public String getTelecomeCompanySelected() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_TELECOM_COMPANY, null);
    }

    public void setDialerCodeSelected(String dialerCodeSelected){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_DAILER_CODE, dialerCodeSelected).apply();
    }
    public String getDialerCodeSelected() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_DAILER_CODE, null);
    }

    public void setPatternSelected(String patternSelected){
        mSharedPreferences.edit().putString(SharedPrefKeyConstants.KEY_PATTERN, patternSelected).apply();
    }
    public String getPatternSelected() {
        return mSharedPreferences.getString(SharedPrefKeyConstants.KEY_PATTERN, null);
    }
}
